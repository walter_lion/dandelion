import React, { Component } from "react";
import {
  Alert,
  Dimensions,
  Image,
  StatusBar,
  View,
  ScrollView,
  Text,
  FlatList,
  DeviceInfo,
  TouchableOpacity
} from "react-native";
import { Button, CheckBox } from "react-native-elements";
import Swiper from "react-native-swiper";
import Overlay from "../../components/custom/Overlay";
const { width, height } = Dimensions.get("window");
const ad1 = require("../../assets/imgs/ad1.jpg");
const ad2 = require("../../assets/imgs/ad2.jpg");
const ad3 = require("../../assets/imgs/ad3.jpg");
const ad4 = require("../../assets/imgs/ad4.jpg");
const ad5 = require("../../assets/imgs/ad5.jpg");
const ad1X = require("../../assets/imgs/ad1-x.jpg");
const ad2X = require("../../assets/imgs/ad2-x.jpg");
const ad3X = require("../../assets/imgs/ad3-x.jpg");
const ad4X = require("../../assets/imgs/ad4-x.jpg");
const ad5X = require("../../assets/imgs/ad5-x.jpg");
const isIphoneX = DeviceInfo.isIPhoneX_deprecated;
const styles = {
  wrapper: {
    // backgroundColor: '#f00'
  },

  slide: {
    flex: 1,
    backgroundColor: "transparent"
  },
  container: {
    position: "relative",
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },

  imgBackground: {
    width,
    height,
    backgroundColor: "transparent",
    position: "absolute"
  },

  image: {
    width,
    height
  }
};

export default class FullScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isVisible: false,
      isAgree: true
    };
  }

  agreeHandle = status => {
    this.setState({
      isAgree: status,
      isVisible: false
    });
  };
  render() {
    const { onPressLogin, onPressSignUp } = this.props;
    const { isVisible, isAgree } = this.state;
    return (
      <View style={styles.container}>
        <Image
          source={require("./imgs/wallpaper.jpg")}
          style={styles.imgBackground}
        />
        <Swiper
          style={styles.wrapper}
          dot={
            <View
              style={{
                backgroundColor: "rgba(255,255,255,.3)",
                width: 4,
                height: 4,
                borderRadius: 2,
                marginLeft: 7,
                marginRight: 7
              }}
            />
          }
          activeDot={
            <View
              style={{
                backgroundColor: "transparent",
                width: 13,
                height: 13,
                borderRadius: 7.5,
                borderWidth: 0.5,
                borderColor: "#fff"
              }}
            >
              <View
                style={{
                  backgroundColor: "#fff",
                  width: 4,
                  height: 4,
                  borderRadius: 2,
                  marginLeft: 4,
                  marginTop: 4
                }}
              />
            </View>
          }
          paginationStyle={{
            bottom: 200
          }}
          loop={false}
        >
          <View style={styles.slide}>
            <Image
              style={styles.image}
              source={isIphoneX ? ad1X : ad1}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.image}
              source={isIphoneX ? ad2X : ad2}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.image}
              source={isIphoneX ? ad3X : ad3}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.image}
              source={isIphoneX ? ad4X : ad4}
              resizeMode="cover"
            />
          </View>
          <View style={styles.slide}>
            <Image
              style={styles.image}
              source={isIphoneX ? ad5X : ad5}
              resizeMode="cover"
            />
          </View>
        </Swiper>
        <View
          style={{
            position: "absolute",
            bottom: 55,
            zIndex: 100,
            width,
            height: 50,
            display: "flex",
            flexDirection: "row",
            justifyContent: "center",
            alignItems: "center"
          }}
        >
          <View
            style={{
              position: "absolute",
              bottom: 0,
              zIndex: 100,
              width: width,
              height: 100,
              display: "flex",
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <View
              style={{
                width: 200,
                height: 100,
                display: "flex",
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <Button
                onPress={() => {
                  isAgree && onPressSignUp();
                }}
                title="Sign Up"
                color="#fff"
                fontSize={16}
                buttonStyle={{
                  width: 120,
                  height: 40,
                  backgroundColor: "rgb(253,92,99)",
                  borderRadius: 4,
                  padding: 0
                }}
              />
              <Button
                onPress={() => {
                  isAgree && onPressLogin();
                }}
                title="Log In"
                color="rgb(253,92,99)"
                fontSize={16}
                buttonStyle={{
                  width: 120,
                  height: 40,
                  backgroundColor: "#fff",
                  borderRadius: 4,
                  padding: 0
                }}
                accessibilityLabel="Learn more about this purple button"
              />
            </View>
          </View>
          <CheckBox
            onPress={() => {
              this.agreeHandle(!isAgree);
            }}
            checkedColor="rgb(253,92,99)"
            containerStyle={{
              margin: 0,
              width: 50,
              height: 45,
              display: "flex",
              flexDirection: "row",
              backgroundColor: "transparent",
              borderWidth: 0
            }}
            checked={isAgree}
          />
          <TouchableOpacity
            onPress={() => {
              this.setState({
                isVisible: true
              });
            }}
          >
            <Text
              style={{
                color: "rgb(145,181,230)",
                textDecorationLine: "underline"
              }}
            >
              Dandelion Content Policy
            </Text>
          </TouchableOpacity>
        </View>
        {isVisible && (
          <View
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
            style={{
              zIndex: 110,
              position: "absolute",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width,
                height,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(255, 255, 255, 0.7)"
              }}
            >
              <View
                style={{
                  width: width - 60,
                  height: height * 0.6,
                  backgroundColor: "rgba(255, 255, 255, 1)"
                }}
              >
                <ScrollView>
                  <View>
                    <Text style={{ textAlign: "center", fontWeight: "bold" }}>
                      TRENDFACTOR CO. LTD.
                    </Text>
                    <Text style={{ textAlign: "center", fontWeight: "bold" }}>
                      PRIVACY POLICY OF DANDELION APP
                    </Text>
                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Privacy Policy
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      Effective date: November 20, 2018 Trendfactor Co. Ltd.
                      ("us", "we", or "our") operates the Dandelion App mobile
                      application (the "Service"). This page informs you of our
                      policies regarding the collection, use, and disclosure of
                      personal data when you use our Service and the choices you
                      have associated with that data. We use your data to
                      provide and improve the Service. By using the Service, you
                      agree to the collection and use of information in
                      accordance with this policy. Unless otherwise defined in
                      this Privacy Policy, terms used in this Privacy Policy
                      have the same meanings as in our Terms and Conditions.
                    </Text>

                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Information Collection And Use
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      We collect several different types of information for
                      various purposes to provide and improve our Service to
                      you.
                    </Text>
                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Types of Data Collected
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      Personal Data While using our Service, we may ask you to
                      provide us with certain personally identifiable
                      information that can be used to contact or identify you
                      ("Personal Data"). Personally identifiable information may
                      include, but is not limited to:
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Email address
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·First name and last name
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Address, State, Province, ZIP/Postal code, City, Country
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Cookies and Usage Data
                    </Text>
                  </View>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Usage Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    When you access the Service by or through a mobile device,
                    we may collect certain information automatically, including,
                    but not limited to, the type of mobile device you use, your
                    mobile device unique ID, the IP address of your mobile
                    device, your mobile operating system, the type of mobile
                    Internet browser you use, unique device identifiers and
                    other diagnostic data ("Usage Data").
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Tracking & Cookies Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We use cookies and similar tracking technologies to track
                    the activity on our Service and hold certain information.
                    Cookies are files with small amount of data which may
                    include an anonymous unique identifier. Cookies are sent to
                    your browser from a website and stored on your device.
                    Tracking technologies also used are beacons, tags, and
                    scripts to collect and track information and to improve and
                    analyze our Service. You can instruct your browser to refuse
                    all cookies or to indicate when a cookie is being sent.
                    However, if you do not accept cookies, you may not be able
                    to use some portions of our Service. Examples of Cookies we
                    use:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Session Cookies. We use Session Cookies to operate our
                    Service.
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Preference Cookies. We use Preference Cookies to remember
                    your preferences and various settings.
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Security Cookies. We use Security Cookies for security
                    purposes.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Use of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Trendfactor Co. Ltd. uses the collected data for various
                    purposes:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide and maintain the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To notify you about changes to our Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To allow you to participate in interactive features of our
                    Service when you choose to do so
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide customer care and support
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide analysis or valuable information so that we can
                    improve the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To monitor the usage of the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To detect, prevent and address technical issues
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Transfer Of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Your information, including Personal Data, may be
                    transferred to — and maintained on — computers located
                    outside of your state, province, country or other
                    governmental jurisdiction where the data protection laws may
                    differ than those from your jurisdiction. If you are located
                    outside Thailand and choose to provide information to us,
                    please note that we transfer the data, including Personal
                    Data, to Thailand and process it there. Your consent to this
                    Privacy Policy followed by your submission of such
                    information represents your agreement to that transfer.
                    Trendfactor Co. Ltd. will take all steps reasonably
                    necessary to ensure that your data is treated securely and
                    in accordance with this Privacy Policy and no transfer of
                    your Personal Data will take place to an organization or a
                    country unless there are adequate controls in place
                    including the security of your data and other personal
                    information.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Security Of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    The security of your data is important to us, but remember
                    that no method of transmission over the Internet, or method
                    of electronic storage is 100% secure. While we strive to use
                    commercially acceptable means to protect your Personal Data,
                    we cannot guarantee its absolute security.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Service Providers
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We may employ third party companies and individuals to
                    facilitate our Service ("Service Providers"), to provide the
                    Service on our behalf, to perform Service-related services
                    or to assist us in analyzing how our Service is used. These
                    third parties have access to your Personal Data only to
                    perform these tasks on our behalf and are obligated not to
                    disclose or use it for any other purpose.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Links To Other Sites
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Our Service may contain links to other sites that are not
                    operated by us. If you click on a third party link, you will
                    be directed to that third party's site. We strongly advise
                    you to review the Privacy Policy of every site you visit. We
                    have no control over and assume no responsibility for the
                    content, privacy policies or practices of any third party
                    sites or services.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Children's Privacy
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Our Service does not address anyone under the age of 18
                    ("Children"). We do not knowingly collect personally
                    identifiable information from anyone under the age of 18. If
                    you are a parent or guardian and you are aware that your
                    Children has provided us with Personal Data, please contact
                    us. If we become aware that we have collected Personal Data
                    from children without verification of parental consent, we
                    take steps to remove that information from our servers.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Changes To This Privacy Policy
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We may update our Privacy Policy from time to time. We will
                    notify you of any changes by posting the new Privacy Policy
                    on this page. We will let you know via email and/or a
                    prominent notice on our Service, prior to the change
                    becoming effective and update the "effective date" at the
                    top of this Privacy Policy. You are advised to review this
                    Privacy Policy periodically for any changes. Changes to this
                    Privacy Policy are effective when they are posted on this
                    page.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Contact Us
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    If you have any questions about this Privacy Policy, please
                    contact us:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·By email: help@trendfactor.co
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·By visiting this page on our website: www.ddlion.me improve
                    the Service
                  </Text>
                </ScrollView>
              </View>
              <Button
                onPress={() => {
                  this.agreeHandle(true);
                }}
                title="Agree"
                color="#fff"
                fontSize={16}
                buttonStyle={{
                  marginTop: 20,
                  width: 120,
                  height: 40,
                  backgroundColor: "rgb(253,92,99)",
                  borderRadius: 4,
                  padding: 0
                }}
                accessibilityLabel="Learn more about this purple button"
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}
