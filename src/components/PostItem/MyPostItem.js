import React, { Component } from "react";
import {
  Dimensions,
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert
} from "react-native";
import { assetRoot } from "../../constants/urlConstant";

export default class MyPostItem extends Component {
  constructor(props) {
    super(props);
  }

  goToProfile = () => {
    const { navigation } = this.props;
    navigation.navigate("OthersProfile");
  };

  goToPoster = () => {
    const { navigation, id } = this.props;
    navigation.navigate("Poster", { postId: id });
  };

  goToEdit = () => {
    const { navigation, id } = this.props;
    navigation.navigate("Post", { postId: id });
  };

  deletePostHandle = () => {
    const { deletePost, id } = this.props;
    Alert.alert(
      "", //提示标题
      `Are you sure to delete this post? `, //提示内容
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        {
          text: "Confirm",
          onPress: () => {
            deletePost(id);
          }
        }
      ]
    );
  };

  render() {
    const { index, width, height, id, postPhotoUrl, postTitle } = this.props;
    const imageW = (width * 162) / 174;
    const imageH = imageW;
    return (
      <View
        style={{
          ...styles.container,
          width,
          height: height,
          marginRight: index / 2 !== 0 ? 7 : 0
        }}
      >
        <TouchableOpacity onPress={this.goToPoster}>
          <Image
            style={{ ...styles.image, width: imageW, height: imageH }}
            source={{ uri: `${assetRoot}/${postPhotoUrl}` }}
            resizeMode="cover"
          />
        </TouchableOpacity>
        <View
          onPress={this.goToEdit}
          style={{
            ...styles.thumbsBox,
            width: (imageW * 50) / 162,
            height: (imageH * 19) / 162,
            top: imageW - 25,
            left: (width - imageW) / 2
          }}
        >
          <TouchableOpacity onPress={this.goToEdit}>
            <View
              style={{
                ...styles.thumbsBox,
                position: "relative",
                width: (imageW * 50) / 162,
                height: (imageH * 19) / 162
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: 12,
                  textAlign: "center",
                  marginRight: 5
                }}
              >
                Edit
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          onPress={this.deletePostHandle}
          style={{
            ...styles.thumbsRightBox,
            width: (imageW * 50) / 162,
            height: (imageH * 19) / 162,
            top: imageW - 25,
            right: (width - imageW) / 2
          }}
        >
          <TouchableOpacity onPress={this.deletePostHandle}>
            <View
              style={{
                ...styles.thumbsRightBox,
                position: "relative",
                width: (imageW * 50) / 162,
                height: (imageH * 19) / 162
              }}
            >
              <Text
                style={{
                  color: "#fff",
                  fontSize: 12,
                  textAlign: "center",
                  marginRight: 5
                }}
              >
                Delete
              </Text>
            </View>
          </TouchableOpacity>
        </View>
        <View
          style={{
            ...styles.postText,
            width: imageW - 20,
            left: (width - imageW) / 2 + 10
          }}
        >
          <TouchableOpacity onPress={this.goToPoster}>
            <Text style={{ fontSize: 13 }}>{postTitle}</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    width: 174,
    height: 238,
    borderRadius: 4,
    marginRight: 7,
    marginTop: 15,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.1
  },
  image: {
    marginTop: -10,
    width: 162,
    height: 162,
    borderRadius: 6,
    backgroundColor: "rgba(0,0,0,0.2)"
  },
  thumbsBox: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minWidth: 39,
    height: 20,
    borderRadius: 2,
    borderBottomLeftRadius: 6,
    backgroundColor: "rgb(253,92,99)"
  },
  thumbsRightBox: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    minWidth: 39,
    height: 20,
    borderRadius: 2,
    borderBottomRightRadius: 6,
    backgroundColor: "rgb(253,92,99)"
  },
  thumbsIoncs: {
    width: 10,
    height: 10,
    margin: 3,
    marginLeft: 5
  },
  postText: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignSelf: "flex-start",
    height: 32,
    marginTop: 8
  },
  bottom: {
    width: "100%",
    height: 46,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    margin: 6,
    backgroundColor: "rgba(0,0,0,0.2)"
  }
});
