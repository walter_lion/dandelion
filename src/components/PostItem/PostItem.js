import React, { Component } from "react";
import {
  Dimensions,
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback
} from "react-native";
import moment from "moment";
import { assetRoot } from "../../constants/urlConstant";

export default class PostItem extends Component {
  constructor(props) {
    super(props);
  }

  goToProfile = () => {
    const {
      navigation,
      postData: { userId }
    } = this.props;
    navigation.navigate("OthersProfile", {
      userId
    });
  };

  goToPoster = () => {
    const { navigation, id, likePost, unLikePost } = this.props;
    navigation.navigate("Poster", {
      postId: id,
      likePost,
      unLikePost
    });
  };

  toggleLikePost = () => {
    const {
      postData: { postId, isLiked },
      unLikePost,
      likePost
    } = this.props;
    if (isLiked === -1 || isLiked === 0) {
      likePost(postId);
    } else if (isLiked === 1) {
      unLikePost(postId);
    }
  };

  render() {
    const { index, width, height, postData } = this.props;
    const imageW = (width * 162) / 174;
    const imageH = imageW;
    const {
      isLiked,
      postCreateTime,
      postId,
      postLikes,
      postPhotoUrl,
      postTitle,
      userId,
      userName,
      avatarPhoto
    } = postData;
    return (
      <View
        style={{
          ...styles.container,
          width,
          height: height + 33,
          marginRight: index / 2 !== 0 ? 7 : 0
        }}
      >
        <TouchableOpacity onPress={this.goToPoster}>
          <Image
            style={{ ...styles.image, width: imageW, height: imageH }}
            source={{ uri: `${assetRoot}/${postPhotoUrl}` }}
            resizeMode="cover"
          />
        </TouchableOpacity>
        <TouchableWithoutFeedback onPress={this.toggleLikePost}>
          <View style={{ ...styles.thumbsBox, top: imageH - 34 }}>
            <Image
              style={styles.thumbsIoncs}
              source={
                isLiked === 1
                  ? require("../../assets/icons/thumbs-up-self.png")
                  : require("../../assets/icons/thumbs-up.png")
              }
              resizeMode="cover"
            />
            <Text
              style={{
                color: "#fff",
                fontSize: 14,
                textAlign: "center",
                marginRight: 5
              }}
            >
              {postLikes}
            </Text>
          </View>
        </TouchableWithoutFeedback>
        <View style={{ ...styles.postText, width: imageW - 20 }}>
          <TouchableOpacity onPress={this.goToPoster}>
            <Text style={{ fontSize: 13, height: "100%" }}>{postTitle}</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bottom}>
          <TouchableOpacity onPress={this.goToProfile}>
            <Image
              style={styles.avatar}
              source={{ uri: `${assetRoot}/${avatarPhoto}` }}
              resizeMode="cover"
            />
          </TouchableOpacity>
          <View style={{ width: imageW - 35, height: 35, marginTop: 8 }}>
            <TouchableOpacity onPress={this.goToProfile}>
              <Text style={{ color: "rgb(20,15,38)", fontSize: 11 }}>
                {userName}
              </Text>
            </TouchableOpacity>
            <Text style={{ color: "rgb(155,155,155)", fontSize: 10 }}>
              {moment(postCreateTime).fromNow()}
            </Text>
          </View>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#fff",
    marginTop: 8,
    width: 174,
    height: 238,
    borderRadius: 4,
    marginRight: 7,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 0.5 },
    shadowOpacity: 0.1
  },
  image: {
    marginTop: -10,
    width: 162,
    height: 162,
    borderRadius: 6,
    backgroundColor: "rgba(0,0,0,0.2)"
  },
  thumbsBox: {
    position: "absolute",
    right: 12,
    top: 130,
    display: "flex",
    flexDirection: "row",
    minWidth: 32,
    height: 18,
    borderRadius: 2,
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  thumbsIoncs: {
    width: 10,
    height: 10,
    margin: 3,
    marginLeft: 5
  },
  postText: {
    width: 150,
    height: 32,
    marginTop: 8
  },
  bottom: {
    width: "100%",
    height: 46,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center"
  },
  avatar: {
    width: 30,
    height: 30,
    borderRadius: 15,
    margin: 6,
    backgroundColor: "rgba(0,0,0,0.2)"
  }
});
