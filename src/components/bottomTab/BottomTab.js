const ICON_HOME = require("../../assets/icons/home3x.png");
const ICON_HOME_SELECTED = require("../../assets/icons/home_selected3x.png");
const ICON_PROFILE = require("../../assets/icons/me.png");
const ICON_PROFILE_SELECTED = require("../../assets/icons/me_selected.png");
const EDIT_POST = require("../../assets/icons/write.png");
import React, { Component } from "react";
import {
  Dimensions,
  Image,
  PixelRatio,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View
} from "react-native";

const dp2px = dp => PixelRatio.getPixelSizeForLayoutSize(dp);
const px2dp = px => PixelRatio.roundToNearestPixel(px);
const { width, height } = Dimensions.get("window");

export default class BottomTab extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { navigation } = this.props;
    const {
      state: { index }
    } = navigation;

    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate("Home");
          }}
        >
          <View style={styles.leftButton}>
            <Image
              source={index === 0 ? ICON_HOME_SELECTED : ICON_HOME}
              style={styles.iconImage}
            />
          </View>
        </TouchableWithoutFeedback>

        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate("Post");
          }}
        >
          <View style={styles.centerWrapper}>
            <Image source={EDIT_POST} style={styles.centerCircle} />
          </View>
        </TouchableWithoutFeedback>
        <TouchableWithoutFeedback
          onPress={() => {
            navigation.navigate("Myprofile");
          }}
        >
          <View style={styles.rightButton}>
            <Image
              source={index === 1 ? ICON_PROFILE_SELECTED : ICON_PROFILE}
              style={styles.iconImage}
            />
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    bottom: 0,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,1)",
    width,
    height: height * 0.12
  },
  centerWrapper: {
    position: "relative",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    marginTop: -50,
    width: 60,
    height: 60,
    borderRadius: 30,
    backgroundColor: "rgba(255,255,255,1)"
  },
  centerCircle: {
    marginTop: 5,
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: "rgb(253,92,99)"
  },
  iconImage: {
    width: 19,
    height: 20
  },
  leftButton: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    top: -8,
    left: 50,
    width: 60,
    height: 60
  },
  rightButton: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    right: 50,
    top: -8,
    width: 60,
    height: 60
  }
});
