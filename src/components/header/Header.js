import React, { Component } from 'react'
import {Dimensions, Image, PixelRatio, StyleSheet, Text ,View} from 'react-native';

const px2dp = px=>PixelRatio.roundToNearestPixel(px);
const { width, height } = Dimensions.get('window')
export default class Header extends Component {
  constructor(props){
    super(props)
  }
  render(){
    const { middleIcon } = this.props
    return(
      <View style={styles.container}>
        <Image source={middleIcon} style={styles.middleIcon} resizeMode='contain'/>
      </View>
    )
  }
}



const styles = StyleSheet.create({
  container: {
    display:'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(255,255,255,0.8)',
    width,
    height: 88,
    borderBottomWidth:1,
    borderBottomColor:'rgba(0,0,0,0.1)',
  },
  middleIcon: {
    marginTop:35,
    width:140,
    height:30,
  }
});
