import * as serviceTool from "../utils/serviceTool";
import md5 from "react-native-md5";
import { SETTING } from "../constants/urlConstant";
import { Alert } from "react-native";
export const changePassword = (currentPassword, newPassword) => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(SETTING.CHANGE_PASSWORD, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        currentPassword: md5.hex_md5(currentPassword),
        newPassword: md5.hex_md5(newPassword)
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const sendToEmail = () => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return storage
      .load({
        key: "loginState",
        autoSync: true
      })
      .then(res => {
        const { username } = res;
        return this.fetch(SETTING.SEND_EMAIL, {
          method: "POST",
          headers: {
            Authorization: `bearer ${access_token}`,
            Accept: "application/json",
            "Content-Type": "application/json"
          },
          body: JSON.stringify({
            email: username
          })
        });
      })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.resolve(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
