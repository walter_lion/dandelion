import * as serviceTool from "../utils/serviceTool";
import { POST } from "../constants/urlConstant";
export const fetchPost = (all, type, page, size) => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.HOME(all, type, page, size), {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const fetchPostDetail = id => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.DETAIL(id), {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const getDefaultTag = () => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.DEFAULT_TAGS, {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const searchTags = options => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.SEARCH_TAGS(options), {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const likePostById = postId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.LIKE_BY_ID, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        postId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const unLikePostById = postId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.LIKE_BY_ID, {
      method: "DELETE",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        postId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const uploadImage = params => {
  let formData = new FormData();
  let files = params.map(path => ({
    uri: path,
    type: "multipart/form-data",
    name: Math.random().toString(16) + "image.jpg"
  }));
  for (item of files) {
    formData.append(item.name, item);
  }
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.UPLOAD_IMMAGE, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        "Content-Type": "multipart/form-data;charset=utf-8"
      },
      body: formData
    })
      .then(res => {
        if (res.status === 200) {
          // Alert.alert(res);
          return res.json();
        } else {
          // Alert.alert(res);
          return Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const creatNewPost = postData => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.CREAT_NEW_POST, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(postData)
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const updatePost = postData => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.CREAT_NEW_POST, {
      method: "PATCH",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(postData)
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const saveTag = tagName => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.SAVE_TAG, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        tagName: tagName
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const deletePost = postId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(POST.DELETE_BY_ID(postId), {
      method: "DELETE",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        postId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
