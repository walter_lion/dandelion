import * as serviceTool from "../utils/serviceTool";
import { PROFILE } from "../constants/urlConstant";

export const fetchUserInfo = userId => {
  return serviceTool.getToken().then(res => {
    const url = PROFILE.USER_INFO + (userId ? "?userId=" + userId : "");
    const access_token = res.value;
    return this.fetch(url, {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const fetchLikedPosts = (userId, page) => {
  return serviceTool.getToken().then(res => {
    const url = PROFILE.FETCH_LIKE_POSTS(userId, page, 10);
    const access_token = res.value;
    return this.fetch(url, {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const fetchIndividalPosts = (userId, page) => {
  return serviceTool.getToken().then(res => {
    const url = PROFILE.FETCH_INDIVIDAL_POSTS(userId, page, 10);
    const access_token = res.value;
    return this.fetch(url, {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
