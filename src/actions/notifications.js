import * as serviceTool from "../utils/serviceTool";
import { NOTIFICATIONS, FOLLOW_USER } from "../constants/urlConstant";

export const fetchNotiFollow = (page, size) => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(NOTIFICATIONS.FOLLOW(page, size), {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const fetchNotiLike = (page, size) => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(NOTIFICATIONS.LIKE(page, size), {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
