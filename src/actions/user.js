import * as serviceTool from "../utils/serviceTool";
import { USER } from "../constants/urlConstant";

export const followingUser = followedUserId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.FOLLOW_AND_UN, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        followedUserId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const unFollowUser = followedUserId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.FOLLOW_AND_UN, {
      method: "DELETE",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        followedUserId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const updateUserInfo = formData => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.UPDATE_INFO, {
      method: "PATCH",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(formData)
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const saveAvatar = path => {
  let formData = new FormData();
  let file = {
    uri: path,
    type: "multipart/form-data",
    name: Math.random().toString(16) + "image.jpg"
  };
  console.log(file);
  formData.append(file.name, file);
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.SAVE_AVATAR, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        "Content-Type": "multipart/form-data;charset=utf-8"
      },
      body: formData
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          return Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const blockUser = blockedUserId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.BLOCK, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        blockedUserId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const unBlockUser = blockedUserId => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.BLOCK, {
      method: "DELETE",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify({
        blockedUserId
      })
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
