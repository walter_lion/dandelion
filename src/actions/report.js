import * as serviceTool from "../utils/serviceTool";
import { REPORT, USER } from "../constants/urlConstant";

export const createReport = reportData => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(REPORT.CREAT_REPORT, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: JSON.stringify(reportData)
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const uploadImage = params => {
  let formData = new FormData();
  let files = params.map(path => ({
    uri: path,
    type: "multipart/form-data",
    name: Math.random().toString(16) + "image.jpg"
  }));
  for (item of files) {
    formData.append(item.name, item);
  }
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(REPORT.UPLOAD_IMMAGE, {
      method: "POST",
      headers: {
        Authorization: `bearer ${access_token}`,
        "Content-Type": "multipart/form-data;charset=utf-8"
      },
      body: formData
    })
      .then(res => {
        if (res.status === 200) {
          // Alert.alert(res);
          return res.json();
        } else {
          // Alert.alert(res);
          return Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

export const acceptEula = reportData => {
  return serviceTool.getToken().then(res => {
    const access_token = res.value;
    return this.fetch(USER.ACCEPT_EULA, {
      method: "PUT",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/json"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return res.json();
        } else {
          Promise.reject(res);
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};
