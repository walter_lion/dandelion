import { PRIMARY, baseRoot, DOMAIN } from "../constants/urlConstant";
import base64 from "base-64";
import CookieManager from "react-native-cookies";
import moment from "moment";
import md5 from "react-native-md5";

const clientId = "test";
const secret = 123456;
const key = `${clientId}:${secret}`;
const basicAuthorization = `Basic ${base64.encode(key)}`;
export const login = (username, password) => {
  const secretedPassword = md5.hex_md5(password);
  return this.fetch(PRIMARY.LOGIN, {
    method: "POST",
    headers: {
      Authorization: basicAuthorization,
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: `grant_type=password&username=${username}&password=${secretedPassword}`
  })
    .then(res => {
      const { status } = res;
      if (status === 200) {
        const resJson = res.json();
        resJson.then(res2 => {
          saveToken(res2.access_token, res2.refresh_token, res2.expires_in);
          storage.save({
            key: "loginState",
            data: {
              username,
              password
            },
            expires: null
          });
        });
        return Promise.resolve(res);
      } else {
        return Promise.reject(res);
      }
    })
    .catch(err => {
      return Promise.reject(err);
    });
};

export const logout = () => {
  return getToken().then(res => {
    const access_token = res.value;
    return this.fetch(PRIMARY.LOGOUT, {
      method: "GET",
      headers: {
        Authorization: `bearer ${access_token}`,
        Accept: "application/json",
        "Content-Type": "application/x-www-form-urlencoded"
      },
      body: null
    })
      .then(res => {
        if (res.status === 200) {
          return Promise.resolve({ code: 200 });
        } else {
          Promise.reject(res);
        }
      })
      .then(res => {
        const { code } = res;
        if (code === 200) {
          return CookieManager.clearAll().then(res => {
            return Promise.resolve({ code: 200 });
          });
        }
      })
      .catch(err => {
        return Promise.reject(err);
      });
  });
};

const generateExpireTime = duration_s => {
  var expiration = moment()
    .add(duration_s, "seconds")
    .format("YYYY-MM-DDTHH:mm:ss.sssZ");
  return expiration;
};

const saveToken = (access_token, refresh_token, expires_in) => {
  var token_expiration = generateExpireTime(expires_in);
  var refresh_expiration = generateExpireTime(1209580);
  CookieManager.set({
    name: "access_token",
    value: access_token,
    domain: DOMAIN,
    origin: baseRoot,
    path: "/",
    version: "1",
    expiration: token_expiration
  })
    .then((res, err) => {
      return CookieManager.set({
        name: "refresh_token",
        value: refresh_token,
        domain: DOMAIN,
        origin: baseRoot,
        path: "/",
        version: "1",
        expiration: refresh_expiration
      });
    })
    .then(res => {
      // do something ..../..
    });
};

const refreshToken = refresh_token => {
  return this.fetch(PRIMARY.REFRESH_TOKEN, {
    method: "POST",
    headers: {
      Authorization: basicAuthorization,
      Accept: "application/json",
      "Content-Type": "application/x-www-form-urlencoded"
    },
    body: `grant_type=refresh_token&refresh_token=${refresh_token}`
  })
    .then(res => {
      const { status } = res;
      if (status === 200) {
        const resJson = res.json();
        return resJson.then(res2 => {
          saveToken(res2.access_token, res2.refresh_token, res2.expires_in);
          return Promise.resolve({ value: res2.access_token });
        });
      } else {
        return Promise.reject(status);
      }
    })
    .catch(err => {
      return Promise.reject(err);
    });
};

export const register = (name, email, password) => {
  password = md5.hex_md5(password);
  return this.fetch(PRIMARY.REGISTER, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json"
    },
    body: JSON.stringify({
      name,
      email,
      password
    })
  })
    .then(res => {
      const { status } = res;
      if (status === 200) {
        const resJson = res.json();
        return resJson;
      }
    })
    .catch(err => {
      return Promise.reject(err);
    });
};

export const getToken = () => {
  return CookieManager.get(baseRoot).then(res2 => {
    if (res2["access_token"]) {
      return Promise.resolve({ value: res2["access_token"] });
    } else {
      return refreshToken(res2["refresh_token"]).catch(err => {
        return storage
          .load({
            key: "loginState",
            autoSync: true
          })
          .then(res => {
            const { username, password } = res;
            return login(username, password);
          })
          .then(res => {
            const { status } = res;
            if (status === 200) {
              return res.json().then(res2 => {
                return { value: res2.access_token };
              });
            }
          });
      });
    }
  });
};
