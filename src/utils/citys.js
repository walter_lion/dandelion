export const citys = [
  {
    Name: "Bangkok (Krung Thep Maha Nakhon)",
    Thai:
      "\u0e01\u0e23\u0e38\u0e07\u0e40\u0e17\u0e1e\u0e21\u0e2b\u0e32\u0e19\u0e04\u0e23"
  },
  { Name: "Nonthaburi", Thai: "\u0e19\u0e19\u0e17\u0e1a\u0e38\u0e23\u0e35" },
  {
    Name: "Nakhon Ratchasima",
    Thai: "\u0e19\u0e04\u0e23\u0e23\u0e32\u0e0a\u0e2a\u0e35\u0e21\u0e32"
  },
  {
    Name: "Chiang Mai",
    Thai: "\u0e40\u0e0a\u0e35\u0e22\u0e07\u0e43\u0e2b\u0e21\u0e48"
  },
  { Name: "Hat Yai", Thai: "\u0e2b\u0e32\u0e14\u0e43\u0e2b\u0e0d\u0e48" },
  {
    Name: "Udon Thani",
    Thai: "\u0e2d\u0e38\u0e14\u0e23\u0e18\u0e32\u0e19\u0e35"
  },
  {
    Name: "Pak Kret",
    Thai: "\u0e1b\u0e32\u0e01\u0e40\u0e01\u0e23\u0e47\u0e14"
  },
  {
    Name: "Chaophraya Surasak",
    Thai:
      "\u0e40\u0e08\u0e49\u0e32\u0e1e\u0e23\u0e30\u0e22\u0e32\u0e2a\u0e38\u0e23\u0e28\u0e31\u0e01\u0e14\u0e34\u0e4c"
  },
  { Name: "Khon Kaen", Thai: "\u0e02\u0e2d\u0e19\u0e41\u0e01\u0e48\u0e19" },
  {
    Name: "Ubon Ratchathani",
    Thai: "\u0e2d\u0e38\u0e1a\u0e25\u0e23\u0e32\u0e0a\u0e18\u0e32\u0e19\u0e35"
  },
  {
    Name: "Nakhon Si Thammarat",
    Thai:
      "\u0e19\u0e04\u0e23\u0e28\u0e23\u0e35\u0e18\u0e23\u0e23\u0e21\u0e23\u0e32\u0e0a"
  },
  {
    Name: "Nakhon Sawan",
    Thai: "\u0e19\u0e04\u0e23\u0e2a\u0e27\u0e23\u0e23\u0e04\u0e4c"
  },
  { Name: "Nakhon Pathom", Thai: "\u0e19\u0e04\u0e23\u0e1b\u0e10\u0e21" },
  {
    Name: "Phitsanulok",
    Thai: "\u0e1e\u0e34\u0e29\u0e13\u0e38\u0e42\u0e25\u0e01"
  },
  { Name: "Pattaya", Thai: "\u0e1e\u0e31\u0e17\u0e22\u0e32" },
  { Name: "Songkhla", Thai: "\u0e2a\u0e07\u0e02\u0e25\u0e32" },
  {
    Name: "Surat Thani",
    Thai:
      "\u0e2a\u0e38\u0e23\u0e32\u0e29\u0e0e\u0e23\u0e4c\u0e18\u0e32\u0e19\u0e35"
  },
  { Name: "Rangsit", Thai: "\u0e23\u0e31\u0e07\u0e2a\u0e34\u0e15" },
  { Name: "Yala", Thai: "\u0e22\u0e30\u0e25\u0e32" },
  { Name: "Phuket", Thai: "\u0e20\u0e39\u0e40\u0e01\u0e47\u0e15" },
  {
    Name: "Samut Prakan",
    Thai: "\u0e2a\u0e21\u0e38\u0e17\u0e23\u0e1b\u0e23\u0e32\u0e01\u0e32\u0e23"
  },
  { Name: "Lampang", Thai: "\u0e25\u0e33\u0e1b\u0e32\u0e07" },
  {
    Name: "Laem Chabang",
    Thai: "\u0e41\u0e2b\u0e25\u0e21\u0e09\u0e1a\u0e31\u0e07"
  },
  {
    Name: "Chiang Rai",
    Thai: "\u0e40\u0e0a\u0e35\u0e22\u0e07\u0e23\u0e32\u0e22"
  },
  { Name: "Trang", Thai: "\u0e15\u0e23\u0e31\u0e07" },
  {
    Name: "Phra Nakhon Si Ayutthaya",
    Thai:
      "\u0e1e\u0e23\u0e30\u0e19\u0e04\u0e23\u0e28\u0e23\u0e35\u0e2d\u0e22\u0e38\u0e18\u0e22\u0e32"
  },
  {
    Name: "Ko Samui",
    Thai: "\u0e40\u0e01\u0e32\u0e30\u0e2a\u0e21\u0e38\u0e22"
  },
  {
    Name: "Samut Sakhon",
    Thai: "\u0e2a\u0e21\u0e38\u0e17\u0e23\u0e2a\u0e32\u0e04\u0e23"
  },
  { Name: "Rayong", Thai: "\u0e23\u0e30\u0e22\u0e2d\u0e07" },
  { Name: "Mae Sot", Thai: "\u0e41\u0e21\u0e48\u0e2a\u0e2d\u0e14" },
  { Name: "Om Noi", Thai: "\u0e2d\u0e49\u0e2d\u0e21\u0e19\u0e49\u0e2d\u0e22" },
  { Name: "Sakon Nakhon", Thai: "\u0e2a\u0e01\u0e25\u0e19\u0e04\u0e23" }
];
