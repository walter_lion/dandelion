export const DOMAIN = "backend.ddlion.me";
export const baseRoot = "https://backend.ddlion.me";
export const assetRoot =
  "https://s3-ap-southeast-1.amazonaws.com/dandeliontest";

const UPDATE_USER_INFO = `/user`;

export const FOLLOW_USER = `/user/following`;

export const PRIMARY = {
  REGISTER: `${baseRoot}/register`,
  LOGIN: `${baseRoot}/oauth/token`,
  REFRESH_TOKEN: `${baseRoot}/oauth/token`,
  LOGOUT: `${baseRoot}/logout`
};

export const USER = {
  FOLLOW_AND_UN: `${baseRoot}/user/following`,
  UPDATE_INFO: `${baseRoot}/user`,
  SAVE_AVATAR: `${baseRoot}/avatar`,
  BLOCK: `${baseRoot}/user/blocking`,
  ACCEPT_EULA: `${baseRoot}/eula`
};

export const POST = {
  HOME: (all, type, page, size) =>
    `${baseRoot}/home?all=${all}&type=${type}&page=${page}&size=${size}`,
  DETAIL: postId => `${baseRoot}/post/${postId}`,
  DEFAULT_TAGS: `${baseRoot}/tag`,
  SEARCH_TAGS: options => `${baseRoot}/tag/search${options}`,
  LIKE_BY_ID: `${baseRoot}/post/like`,
  UPLOAD_IMMAGE: `${baseRoot}/post/photo`,
  CREAT_NEW_POST: `${baseRoot}/post`,
  SAVE_TAG: `${baseRoot}/tag`,
  DELETE_BY_ID: postId => `${baseRoot}/post/${postId}`
};

export const PROFILE = {
  USER_INFO: `${baseRoot}/profile`,
  FETCH_LIKE_POSTS: (userId, page, size) =>
    `${baseRoot}/profile/like?userId=${userId}&page=${page}&size=${size}`,
  FETCH_INDIVIDAL_POSTS: (userId, page, size) =>
    `${baseRoot}/profile/post?userId=${userId}&page=${page}&size=${size}`
};

export const NOTIFICATIONS = {
  FOLLOW: (page, size) =>
    `${baseRoot}/notification/follower?page=${page}&size=${size}`,
  LIKE: (page, size) =>
    `${baseRoot}/notification/like?page=${page}&size=${size}`
};

export const SETTING = {
  CHANGE_PASSWORD: `${baseRoot}/password`,
  SEND_EMAIL: `${baseRoot}/email/forgot_password`
};

export const REPORT = {
  UPLOAD_IMMAGE: `${baseRoot}/report/photo`,
  CREAT_REPORT: `${baseRoot}/report`
};
