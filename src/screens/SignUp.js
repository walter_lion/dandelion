import React, { Component } from "react";
import { Dimensions, Image, StyleSheet, Text, View } from "react-native";
import { Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import * as serviceTool from "../utils/serviceTool";
const registerBg = require("../assets/imgs/registerBj.jpg");
const { width, height } = Dimensions.get("window");
const fontSize = 16;
export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      isLoading: false,
      errorObj: { email: 0, name: 0, password: 0 }
    };
  }

  onInputUsername = event => {
    const username = event.nativeEvent.text;
    let errorObj = this.state.errorObj;
    errorObj.name = 0;
    this.setState({
      username: username.trim(),
      errorObj
    });
  };
  onInputEmail = event => {
    const email = event.nativeEvent.text;
    let errorObj = this.state.errorObj;
    errorObj.email = 0;
    this.setState({
      email,
      errorObj
    });
  };
  onInputPassword = event => {
    const password = event.nativeEvent.text;
    let errorObj = this.state.errorObj;
    errorObj.password = 0;
    this.setState({
      password
    });
  };

  handleSubmit = () => {
    let { username, email, password, isLoading, errorObj } = this.state;
    const { navigation } = this.props;
    if (username === "") {
      errorObj.name = "Username shouldn't be empty !";
      this.setState({
        errorObj
      });
      return;
    } else if (email === "") {
      errorObj.email = "Mailbox shouldn't be empty !";
      this.setState({
        errorObj
      });
      return;
    } else if (password === "") {
      errorObj.password = "Password shouldn't be empty !";
      this.setState({
        errorObj
      });
      return;
    }
    this.setState({
      isLoading: true
    });
    serviceTool.register(username, email, password).then(res => {
      const { code } = res;
      this.setState({
        isLoading: false
      });
      if (code === 200) {
        // success
        navigation.navigate("Login");
      } else if (code === 1004) {
        //multi
        errorObj.email = "The mailbox has been registered ！";
        this.setState({
          errorObj
        });
      } else {
        errorObj.email = "Registration failed. Please try again ！";
        this.setState({
          errorObj
        });
      }
    });
  };
  goToLogin = () => {
    const { navigation } = this.props;
    navigation.navigate("Login");
  };
  render() {
    const { errorObj, isLoading } = this.state;
    return (
      <View style={styles.container}>
        <Image source={registerBg} style={styles.imgBackground} />
        <View style={styles.formView}>
          <View style={styles.headLabel}>
            <Text style={styles.welcome}>Sign Up</Text>
          </View>
          <TextField
            label="Name"
            containerStyle={{ margin: 5 }}
            inputContainerStyle={styles.inputContainer}
            baseColor={"rgba(255,255,255,0.5)"}
            tintColor={"#fff"}
            textColor={"#fff"}
            error={errorObj.name !== 0 ? errorObj.name : ""}
            onChange={this.onInputUsername}
          />
          <TextField
            label="Email"
            containerStyle={{ margin: 5 }}
            inputContainerStyle={styles.inputContainer}
            baseColor={"rgba(255,255,255,0.5)"}
            tintColor={"#fff"}
            textColor={"#fff"}
            error={errorObj.email !== 0 ? errorObj.email : ""}
            onChange={this.onInputEmail}
          />
          <TextField
            label="Password"
            secureTextEntry={true}
            containerStyle={{ margin: 5 }}
            inputContainerStyle={styles.inputContainer}
            baseColor={"rgba(255,255,255,0.5)"}
            tintColor={"#fff"}
            textColor={"#fff"}
            error={errorObj.password !== 0 ? errorObj.password : ""}
            onChange={this.onInputPassword}
          />
          <Button
            loading={isLoading}
            onPress={this.handleSubmit}
            title="Sign Up"
            fontSize={fontSize}
            color="#fff"
            buttonStyle={styles.button}
          />
        </View>
        <View style={styles.tipInfo}>
          <Text style={{ fontSize, color: "#fff" }}>
            Already have an account?{" "}
          </Text>
          <Text
            style={{ fontSize, color: "rgb(253,92,99)" }}
            onPress={this.goToLogin}
          >
            Log In
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  button: {
    marginTop: 20,
    padding: 0,
    width: 263,
    height: 40,
    alignSelf: "center",
    backgroundColor: "rgb(253,92,99)",
    borderRadius: 4
  },
  imgBackground: {
    width,
    height,
    backgroundColor: "transparent",
    position: "absolute"
  },
  headLabel: {
    display: "flex",
    alignSelf: "flex-start",
    width: 263,
    height: 50
  },
  formView: {
    marginTop: -100,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: 263,
    height: 300
  },
  inputContainer: {
    width: 263,
    height: 60
  },
  welcome: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#fff",
    textAlign: "left",
    margin: 0
  },
  tipInfo: {
    position: "absolute",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    textAlign: "center",
    color: "#333333",
    bottom: 61
  }
});
