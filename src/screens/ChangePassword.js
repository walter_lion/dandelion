// icons
const back = require("../assets/icons/back.png");
const down_arrow = require("../assets/icons/down_arrow.png");
// components
import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import { TextField } from "react-native-material-textfield";
import Picker from "react-native-picker";

// actions　
import { changePassword as changePasswordHandle } from "../actions/setting";

const restrictW = width - 40;

const { width, height } = Dimensions.get("window");
var fetchParams = {
  first: 6,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({ styles, opacity, leftEvent, rightEvent, title }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image source={back} />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          >
            SAVE
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const Arrow = () => <Image source={down_arrow} />;
const ImageItem = ({ url, selected, index }) => {
  return (
    <View style={styles.imageItem}>
      <View style={styles.imageSelected}>
        <View style={styles.dot}>
          <Text
            style={{
              width: 15,
              height: 15,
              textAlign: "center",
              color: "rgb(253,92,99)"
            }}
          >
            {index}
          </Text>
        </View>
      </View>
      <Image
        style={{ width: 85, height: 85, borderRadius: 4 }}
        source={{ uri: url }}
      />
    </View>
  );
};

export default class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentPassword: "",
      confirmPassword: "",
      newPassword: "",
      currentPasswordError: "",
      confirmError: ""
    };
  }

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  savehandle = () => {
    const { navigation } = this.props;
    const { currentPassword, confirmPassword, newPassword } = this.state;
    if (newPassword === confirmPassword && currentPassword !== "") {
      changePasswordHandle(currentPassword, newPassword).then(res => {
        const { code, data } = res;
        if (code === 200) {
          navigation.goBack();
        } else if (code === 1031) {
          this.setState({
            confirmError: "password is inconsistent !",
            currentPasswordError: "password is inconsistent !"
          });
        }
      });
    } else if (currentPassword === "") {
      this.setState({
        currentPasswordError: "password should not be empty !",
        confirmError: ""
      });
    } else {
      this.setState({
        confirmError: "password is inconsistent !",
        currentPasswordError: ""
      });
    }
  };

  inputOldPassword = currentPassword => {
    this.setState({
      currentPassword
    });
  };

  inputNewPassword = newPassword => {
    this.setState({
      newPassword
    });
  };

  inputConfirmPassword = confirmPassword => {
    this.setState({
      confirmPassword,
      confirmError: ""
    });
  };

  forgetpassword = () => {
    const { navigation } = this.props;
    navigation.navigate("ForgetPassword");
  };

  render() {
    const {
      currentPassword,
      confirmPassword,
      newPassword,
      currentPasswordError,
      confirmError
    } = this.state;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Change Password"}
          leftEvent={this.goBack}
          rightEvent={this.savehandle}
        />
        <ScrollView style={styles.wrapper}>
          <View style={styles.selectContainer}>
            <TextField
              label="Current Password"
              containerStyle={{ marginBottom: 5 }}
              inputContainerStyle={styles.inputContainer}
              baseColor={"rgb(211,211,211)"}
              tintColor={"rgb(13,14,21)"}
              textColor={"rgb(13,14,21)"}
              value={currentPassword}
              error={currentPasswordError}
              errorColor={"rgb(253,92,99)"}
              onChangeText={this.inputOldPassword}
            />
            <TextField
              label="New Password"
              containerStyle={{ marginBottom: 5 }}
              inputContainerStyle={styles.inputContainer}
              baseColor={"rgb(211,211,211)"}
              tintColor={"rgb(13,14,21)"}
              textColor={"rgb(13,14,21)"}
              value={newPassword}
              error={confirmError}
              errorColor={"rgb(253,92,99)"}
              onChangeText={this.inputNewPassword}
            />
            <TextField
              label="Confirm New Password"
              containerStyle={{ marginBottom: 5 }}
              inputContainerStyle={styles.inputContainer}
              baseColor={"rgb(211,211,211)"}
              tintColor={"rgb(13,14,21)"}
              textColor={"rgb(13,14,21)"}
              value={confirmPassword}
              error={confirmError}
              errorColor={"rgb(253,92,99)"}
              onChangeText={this.inputConfirmPassword}
            />
            <View style={styles.formItem}>
              <TouchableOpacity onPress={this.forgetpassword}>
                <Text style={styles.formItemText}>Forget password?</Text>
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 18,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    width: restrictW - 36,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  openCamera: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgb(253,92,99)"
  },
  mainContainer: {
    position: "relative",
    margin: 20,
    width: 200,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  imageMask: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 200,
    height: 200,
    zIndex: 2,
    backgroundColor: "rgba(253,92,99,0.7)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  photoIcon: {
    width: 32,
    height: 27
  },
  selectContainer: {
    width,
    height: 500,
    display: "flex",
    alignItems: "center"
  },
  formView: {
    width: 263,
    height: 300,
    display: "flex",
    alignItems: "center"
  },
  imageItem: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85
  },
  imageSelected: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    left: 0,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgba(253,92,99,0.5)",
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    flex: 1,
    width,
    marginTop: 10,
    backgroundColor: "#fff"
  },
  formItem: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-end",
    marginTop: 15,
    paddingRight: 16,
    width,
    height: 60,
    backgroundColor: "#fff"
  },
  formItemText: {
    fontSize: 14,
    color: "rgb(253,92,99)"
  },
  inputContainer: {
    width: width - 32,
    height: 55
  }
});
