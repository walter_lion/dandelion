// icons
const camera = require("../assets/icons/camera.png");
const close = require("../assets/icons/cancel.png");
const down_arrow = require("../assets/icons/down_arrow.png");

// constanst
import { citys } from "../utils/citys";
import { assetRoot } from "../constants/urlConstant";

// others
import DeviceInfo from "react-native-device-info";
import Picker from "react-native-picker";

// components
import Overlay from "../components/custom/Overlay";
import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableNativeFeedback,
  View,
  ActivityIndicator
} from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import { TextField } from "react-native-material-textfield";
import PhoneInput from "react-native-phone-input";
import CountryPicker, {
  getAllCountries
} from "react-native-country-picker-modal";

// actions
import { saveAvatar, updateUserInfo } from "../actions/user";

const restrictW = width - 40;

/*
<TextField
  label="Phone"
  keyboardType="numeric"
  inputContainerStyle={styles.inputContainer}
  baseColor={"rgb(211,211,211)"}
  tintColor={"rgb(13,14,21)"}
  textColor={"rgb(13,14,21)"}
  value={"test"}
  onChangeText={phone => {}}
  renderAccessory={() => (
    <View>

    </View>
  )}
/>
*/

const { width, height } = Dimensions.get("window");
var fetchParams = {
  first: 6,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({ styles, opacity, leftEvent, rightEvent, title }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          >
            SAVE
          </Text>
        </View>
      </TouchableOpacity>
    </View>
  );
};

const Arrow = () => <Image source={down_arrow} />;
const ImageItem = ({ url, selected, index }) => {
  return (
    <View style={styles.imageItem}>
      <View style={styles.imageSelected}>
        <View style={styles.dot}>
          <Text
            style={{
              width: 15,
              height: 15,
              textAlign: "center",
              color: "rgb(253,92,99)"
            }}
          >
            {index}
          </Text>
        </View>
      </View>
      <Image
        style={{ width: 85, height: 85, borderRadius: 4 }}
        source={{ uri: url }}
      />
    </View>
  );
};

const formatDate = array => {
  const num = array.map(item => item.split(" ")[0]);
  let arr = [];
  arr.push(num[1]);
  arr.push(num[2]);
  arr.push(num[0]);
  return arr.join("/");
};

const deFormatDate = string => {
  let array = string.split("/").reverse();
  let arr = [];
  arr[0] = array[0] + " Year";
  arr[1] = array[2] + " Month";
  arr[2] = array[1] + " Day";
  return arr;
};
export default class EditProfile extends Component {
  constructor() {
    super();
    let userLocaleCountryCode = DeviceInfo.getDeviceCountry();
    userLocaleCountryCode = "th";
    this.state = {
      userLocaleCountryCode,
      imageList: [],
      avatarPhoto: null,
      name: "",
      email: "",
      phone: "",
      birthday: "",
      gender: "",
      location: "",
      needUpdate: {},
      errorObj: { name: 0, email: 0 },
      fetching: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    var _that = this;

    const {
      state: { params }
    } = navigation;

    if (params.userInfo) {
      const {
        avatarPhoto,
        name,
        email,
        phone,
        birthday,
        gender,
        location
      } = params.userInfo;
      this.setState({
        avatarPhoto,
        name,
        email,
        phone,
        birthday: birthday !== null ? deFormatDate(birthday) : undefined,
        gender: gender !== null ? [gender] : undefined,
        location
      });
    }
  }

  wakeUpCamera = e => {
    return ImagePicker.openPicker({
      width: 1350,
      height: 1350,
      cropping: true,
      compressImageQuality: 1
    }).then(image => {
      return image.path;
    });
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  saveHandle = () => {
    const { navigation } = this.props;
    const {
      state: { params }
    } = navigation;
    let { needUpdate, errorObj } = this.state;
    this.refs.name.blur();
    this.refs.email.blur();
    let emailRegx = /^([\w-_]+(?:\.[\w-_]+)*)@((?:[a-z0-9]+(?:-[a-zA-Z0-9]+)*)+\.[a-z]{2,6})$/i;
    if (needUpdate.name === "") {
      errorObj.name = "Username shouldn't be empty !";
      this.setState({
        errorObj
      });
      return;
    } else if (needUpdate.email === "") {
      errorObj.email = "Email shouldn't be empty !";
      this.setState({
        errorObj
      });
      return;
    } else if (needUpdate.email && emailRegx.exec(needUpdate.email) === null) {
      errorObj.email = "Please type correct email !";
      this.setState({
        errorObj
      });
      return;
    }
    this.setState({
      fetching: true
    });
    updateUserInfo(needUpdate).then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState(
          {
            fetching: false
          },
          () => {
            params.refreshHandle();
            this.goNext();
          }
        );
      }
    });
  };

  goNext = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  _createAreaData() {
    let data = [];
    data = citys.map(item => item.Name);
    return data;
  }

  _createDateData() {
    let date = [];
    for (let i = 1970; i < 2020; i++) {
      let month = [];
      for (let j = 1; j < 13; j++) {
        let day = [];
        if (j === 2) {
          for (let k = 1; k < 29; k++) {
            day.push(k + " Day");
          }
          //Leap day for years that are divisible by 4, such as 2000, 2004
          if (i % 4 === 0) {
            day.push(29 + " Day");
          }
        } else if (j in { 1: 1, 3: 1, 5: 1, 7: 1, 8: 1, 10: 1, 12: 1 }) {
          for (let k = 1; k < 32; k++) {
            day.push(k + " Day");
          }
        } else {
          for (let k = 1; k < 31; k++) {
            day.push(k + " Day");
          }
        }
        let _month = {};
        _month[j + " Month"] = day;
        month.push(_month);
      }
      let _date = {};
      _date[i + " Year"] = month;
      date.push(_date);
    }
    return date;
  }

  _showDatePicker = () => {
    let { birthday, needUpdate } = this.state;
    let initDate = birthday
      ? birthday
      : [
          new Date().getFullYear() + " Year",
          new Date().getMonth() + 1 + " Month",
          new Date().getDate() + " Day"
        ];
    Picker.init({
      selectedValue: initDate,
      pickerData: this._createDateData(),
      pickerFontColor: [0, 0, 0, 1],
      pickerTitleText: "",
      onPickerConfirm: (pickedValue, pickedIndex) => {
        needUpdate.birthday = formatDate(pickedValue);
        this.setState({
          birthday: pickedValue,
          needUpdate
        });
      },
      onPickerCancel: (pickedValue, pickedIndex) => {},
      onPickerSelect: (pickedValue, pickedIndex) => {
        needUpdate.birthday = pickedValue;
        this.setState({
          birthday: pickedValue,
          needUpdate
        });
      }
    });
    Picker.show();
  };
  _showAreaPicker = () => {
    let { location, needUpdate } = this.state;
    Picker.init({
      pickerData: this._createAreaData(),
      pickerTitleText: "",
      selectedValue: [
        location === null ? "Bangkok (Krung Thep Maha Nakhon)" : location
      ],
      onPickerConfirm: pickedValue => {
        needUpdate.location = pickedValue[0];
        this.setState({
          location: pickedValue[0],
          needUpdate
        });
      },
      onPickerCancel: pickedValue => {},
      onPickerSelect: pickedValue => {
        needUpdate.location = pickedValue[0];
        this.setState({
          location: pickedValue[0],
          needUpdate
        });
      }
    });
    Picker.show();
  };
  _showOptionsPicker() {
    let { gender, needUpdate } = this.state;
    Picker.init({
      pickerData: ["Male", "Female"],
      pickerFontColor: [0, 0, 0, 1],
      pickerTitleText: "",
      onPickerConfirm: (pickedValue, pickedIndex) => {
        needUpdate.gender = pickedValue[0];
        this.setState({
          gender: pickedValue,
          needUpdate
        });
      },
      onPickerCancel: (pickedValue, pickedIndex) => {},
      onPickerSelect: (pickedValue, pickedIndex) => {
        needUpdate.gender = pickedValue[0];
        this.setState({
          gender: pickedValue,
          needUpdate
        });
      }
    });
    Picker.show();
  }

  chooseAndUpload = () => {
    this.wakeUpCamera()
      .then(res => {
        return saveAvatar(res);
      })
      .then(res => {
        const { code, data } = res;
        if (code === 200) {
          this.setState({
            avatarPhoto: data
          });
        }
      });
  };

  changeName = event => {
    const name = event.nativeEvent.text;
    let { needUpdate, errorObj } = this.state;
    needUpdate.name = name;
    errorObj.name = 0;
    this.setState({
      name,
      needUpdate,
      errorObj
    });
  };

  changeEmail = event => {
    const email = event.nativeEvent.text;
    let { needUpdate, errorObj } = this.state;
    needUpdate.email = email;
    errorObj.email = 0;
    this.setState({
      email,
      needUpdate,
      errorObj
    });
  };

  changePhone = phone => {
    let { needUpdate } = this.state;
    needUpdate.phone = phone;
    this.setState({
      phone,
      needUpdate
    });
  };

  render() {
    const {
      imageList,
      userLocaleCountryCode,
      avatarPhoto,
      name,
      email,
      phone,
      birthday,
      gender,
      location,
      errorObj,
      fetching
    } = this.state;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Edit Profile"}
          leftEvent={this.goBack}
          rightEvent={this.saveHandle}
        />
        <ScrollView style={styles.wrapper}>
          <TouchableWithoutFeedback onPress={this.chooseAndUpload}>
            <View style={styles.mainContainer}>
              {!avatarPhoto && (
                <View style={styles.imageMask}>
                  <Image style={styles.photoIcon} source={camera} />
                </View>
              )}
              <Image
                style={{ width: 200, height: 200, borderRadius: 4 }}
                source={{ uri: `${assetRoot}/${avatarPhoto}` }}
              />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.selectContainer}>
            <View style={styles.formView}>
              <TextField
                ref="name"
                label="Name"
                inputContainerStyle={styles.inputContainer}
                baseColor={"rgb(211,211,211)"}
                tintColor={"rgb(13,14,21)"}
                textColor={"rgb(13,14,21)"}
                value={name}
                maxLength={16}
                onChange={this.changeName}
              />
              <TextField
                ref="email"
                label="Email"
                error={errorObj.email !== 0 ? errorObj.email : ""}
                inputContainerStyle={styles.inputContainer}
                baseColor={"rgb(211,211,211)"}
                tintColor={"rgb(13,14,21)"}
                textColor={"rgb(13,14,21)"}
                value={email}
                onChange={this.changeEmail}
              />

              <PhoneInput
                ref="phone"
                keyboardType="numeric"
                style={{ ...styles.inputContainer, marginTop: 20 }}
                textStyle={{
                  borderBottomWidth: 1,
                  borderBottomColor: "rgba(0,0,0,0.1)"
                }}
                value={phone}
                initialCountry={userLocaleCountryCode}
                onChangePhoneNumber={this.changePhone}
              />
              <TouchableOpacity onPress={this._showDatePicker.bind(this)}>
                <TextField
                  label="Birthday"
                  inputContainerStyle={styles.inputContainer}
                  baseColor={"rgb(211,211,211)"}
                  tintColor={"rgb(13,14,21)"}
                  textColor={"rgb(13,14,21)"}
                  value={Array.isArray(birthday) ? formatDate(birthday) : ""}
                  editable={false}
                  onFocus={this._showDatePicker.bind(this)}
                  renderAccessory={Arrow}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._showOptionsPicker.bind(this)}>
                <TextField
                  label="Gender"
                  inputContainerStyle={styles.inputContainer}
                  baseColor={"rgb(211,211,211)"}
                  tintColor={"rgb(13,14,21)"}
                  textColor={"rgb(13,14,21)"}
                  value={
                    Array.isArray(gender)
                      ? gender.map(item => item.split(" ")[0])[0]
                      : ""
                  }
                  editable={false}
                  onFocus={this._showOptionsPicker.bind(this)}
                  renderAccessory={Arrow}
                />
              </TouchableOpacity>
              <TouchableOpacity onPress={this._showAreaPicker.bind(this)}>
                <TextField
                  label="Location"
                  inputContainerStyle={styles.inputContainer}
                  baseColor={"rgb(211,211,211)"}
                  tintColor={"rgb(13,14,21)"}
                  textColor={"rgb(13,14,21)"}
                  value={location}
                  editable={false}
                  onFocus={this._showAreaPicker.bind(this)}
                  renderAccessory={Arrow}
                />
              </TouchableOpacity>
            </View>
          </View>
        </ScrollView>
        {fetching && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 18,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    width: restrictW - 36,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  openCamera: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgb(253,92,99)"
  },
  mainContainer: {
    position: "relative",
    margin: 20,
    width: 200,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  imageMask: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 200,
    height: 200,
    zIndex: 2,
    backgroundColor: "rgba(253,92,99,0.7)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  photoIcon: {
    width: 32,
    height: 27
  },
  selectContainer: {
    width,
    height: 500,
    display: "flex",
    alignItems: "center"
  },
  formView: {
    width: 263,
    height: 300,
    display: "flex",
    alignItems: "center"
  },
  inputContainer: {
    width: 263,
    height: 60
  },
  imageItem: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85
  },
  imageSelected: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    left: 0,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgba(253,92,99,0.5)",
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    flex: 1,
    width,
    height
  }
});
