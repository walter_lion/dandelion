// icons
const close = require("../../assets/icons/cancel.png");
const down_arrow = require("../../assets/icons/down_arrow.png");

// components
import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  View
} from "react-native";
import Overlay from "../../components/custom/Overlay";
import { Button, ListItem } from "react-native-elements";
import { createReport, uploadImage, acceptEula } from "../../actions/report";
const { width, height } = Dimensions.get("window");

const restrictW = width - 40;

const pushNoti = () => (
  <View style={styles.formItem2}>
    <Text>Push-notifications</Text>
    <Switch
      value={true}
      onValueChange={() => {}}
      onTintColor="rgb(44,197,50)"
      thumbTintColor="#fff"
    />
  </View>
);
var fetchParams = {
  first: 6,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({
  styles,
  opacity,
  leftEvent,
  rightShow,
  rightEvent,
  title
}) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      {rightShow ? (
        <TouchableOpacity onPress={rightEvent}>
          <View style={styles.share}>
            <Text
              style={{
                color: "rgb(253,92,99)",
                fontSize: 14,
                fontWeight: "bold"
              }}
            >
              Submit
            </Text>
          </View>
        </TouchableOpacity>
      ) : (
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          />
        </View>
      )}
    </View>
  );
};

const Arrow = () => <Image source={down_arrow} />;
const ImageItem = ({ url, selected, index }) => {
  return (
    <View style={styles.imageItem}>
      <View style={styles.imageSelected}>
        <View style={styles.dot}>
          <Text
            style={{
              width: 15,
              height: 15,
              textAlign: "center",
              color: "rgb(253,92,99)"
            }}
          >
            {index}
          </Text>
        </View>
      </View>
      <Image
        style={{ width: 85, height: 85, borderRadius: 4 }}
        source={{ uri: url }}
      />
    </View>
  );
};

export default class Report extends Component {
  constructor() {
    super();
    this.state = {
      isVisible: false,
      tip: null,
      isLoading: false,
      selectedIndex: null,
      reasonList: [
        {
          description: "Hate Speech:",
          detail: "Race/Religion/Cender/Native/LGBT/Ethnic Orgin Discrimination"
        },
        {
          description: "Sexual Orientiation"
        },
        {
          description: "Violence"
        },
        {
          description: "Harassement or Bullying"
        },
        {
          description: "Copyright Violation"
        },
        {
          description: "Spam"
        }
      ]
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    var _that = this;
  }

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  selectReasonHandle = selectedIndex => {
    const { selectedIndex: preInex } = this.state;
    this.setState({
      selectedIndex: preInex === selectedIndex ? null : selectedIndex
    });
  };

  showPolicyHandle = () => {
    this.setState({
      isVisible: true
    });
  };

  agreeHandle = () => {
    acceptEula().then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState({
          isVisible: false
        });
      }
    });
  };

  submitReaseon = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props;
    const { selectedIndex, reasonList } = this.state;
    let idObj = {};
    if (params.postId) {
      idObj.postId = params.postId;
    } else if (params.userId) {
      idObj.userId = params.userId;
    }
    const targetItem = reasonList[selectedIndex];
    const reasonText =
      targetItem.description + (targetItem.detail ? targetItem.detail : "");
    this.setState({
      isLoading: true
    });
    createReport(
      Object.assign(
        {},
        {
          reason: reasonText
        },
        idObj
      )
    ).then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState({
          isLoading: false
        });
        this.goBack();
      }
    });
  };

  otherReaseonHandle = () => {
    const { navigation } = this.props;
    const params = navigation.state.params;
    const { userId, postId } = params;
    let idObj = {};
    if (userId) {
      idObj.userId = userId;
    } else if (postId) {
      idObj.postId = postId;
    }

    this.selectReasonHandle(10);
    navigation.navigate("OtherReport", idObj);
  };

  render() {
    const { isVisible, tip, isLoading, reasonList, selectedIndex } = this.state;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Report"}
          leftEvent={this.goBack}
          rightShow={selectedIndex !== null && selectedIndex !== 10}
          rightEvent={this.submitReaseon}
        />
        <View style={styles.wrapper}>
          <View style={styles.intro}>
            <Text style={styles.introText}>
              please select a reason for you complaint before you submit it.
            </Text>
          </View>
          <View>
            {reasonList.map((reason, index) => (
              <TouchableOpacity
                onPress={this.selectReasonHandle.bind(this, index)}
              >
                <View
                  style={Object.assign(
                    {},
                    styles.itemContainer,
                    selectedIndex === index
                      ? { backgroundColor: "rgb(253,92,99)" }
                      : {}
                  )}
                >
                  <View style={styles.itemWrapper}>
                    <Text
                      style={Object.assign(
                        {},
                        styles.itemTitle,
                        selectedIndex === index
                          ? { color: "rgb(255,255,255)" }
                          : {}
                      )}
                    >
                      {reason.description}
                    </Text>
                    {reason.detail && (
                      <Text
                        style={Object.assign(
                          {},
                          styles.itemText,
                          selectedIndex === index
                            ? { color: "rgb(255,255,255)" }
                            : {}
                        )}
                      >
                        {reason.detail}
                      </Text>
                    )}
                  </View>
                </View>
              </TouchableOpacity>
            ))}
            <TouchableOpacity onPress={this.otherReaseonHandle}>
              <View
                style={Object.assign(
                  {},
                  styles.itemContainer,
                  {
                    borderBottomWidth: 1,
                    borderBottomColor: "rgba(0,0,0,0.1)"
                  },
                  selectedIndex === 10
                    ? { backgroundColor: "rgb(253,92,99)" }
                    : {}
                )}
              >
                <View
                  style={{
                    ...styles.itemWrapper,
                    borderBottomWidth: 0
                  }}
                >
                  <Text
                    style={Object.assign(
                      {},
                      styles.itemTitle,
                      selectedIndex === 10 ? { color: "rgb(255,255,255)" } : {}
                    )}
                  >
                    Other
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
            <View
              style={{
                marginLeft: 22,
                width: width - 22,
                height: 50,
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-start",
                alignItems: "center"
              }}
            >
              <Text
                style={{
                  color: "rgb(155,155,155)",
                  fontSize: 16,
                  marginRight: 10
                }}
              >
                Read the{" "}
              </Text>
              <TouchableOpacity onPress={this.showPolicyHandle}>
                <Text
                  style={{
                    color: "rgb(145,181,230)",
                    fontSize: 16,
                    textDecorationLine: "underline"
                  }}
                >
                  Dandelion Content Policy
                </Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        {isLoading && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
        {isVisible && (
          <View
            style={{
              zIndex: 110,
              position: "absolute",
              display: "flex",
              justifyContent: "center",
              alignItems: "center"
            }}
          >
            <View
              style={{
                width,
                height,
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: "rgba(255, 255, 255,1)"
              }}
            >
              <View
                style={{
                  width: width - 60,
                  height: height * 0.6,
                  backgroundColor: "rgba(255, 255, 255, 1)"
                }}
              >
                <ScrollView>
                  <View>
                    <Text style={{ textAlign: "center", fontWeight: "bold" }}>
                      TRENDFACTOR CO. LTD.
                    </Text>
                    <Text style={{ textAlign: "center", fontWeight: "bold" }}>
                      PRIVACY POLICY OF DANDELION APP
                    </Text>
                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Privacy Policy
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      Effective date: November 20, 2018 Trendfactor Co. Ltd.
                      ("us", "we", or "our") operates the Dandelion App mobile
                      application (the "Service"). This page informs you of our
                      policies regarding the collection, use, and disclosure of
                      personal data when you use our Service and the choices you
                      have associated with that data. We use your data to
                      provide and improve the Service. By using the Service, you
                      agree to the collection and use of information in
                      accordance with this policy. Unless otherwise defined in
                      this Privacy Policy, terms used in this Privacy Policy
                      have the same meanings as in our Terms and Conditions.
                    </Text>

                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Information Collection And Use
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      We collect several different types of information for
                      various purposes to provide and improve our Service to
                      you.
                    </Text>
                    <Text
                      style={{
                        marginTop: 5,
                        textAlign: "left",
                        fontWeight: "bold",
                        marginLeft: 3
                      }}
                    >
                      Types of Data Collected
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      Personal Data While using our Service, we may ask you to
                      provide us with certain personally identifiable
                      information that can be used to contact or identify you
                      ("Personal Data"). Personally identifiable information may
                      include, but is not limited to:
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Email address
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·First name and last name
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Address, State, Province, ZIP/Postal code, City, Country
                    </Text>
                    <Text style={{ textAlign: "left", marginLeft: 3 }}>
                      ·Cookies and Usage Data
                    </Text>
                  </View>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Usage Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    When you access the Service by or through a mobile device,
                    we may collect certain information automatically, including,
                    but not limited to, the type of mobile device you use, your
                    mobile device unique ID, the IP address of your mobile
                    device, your mobile operating system, the type of mobile
                    Internet browser you use, unique device identifiers and
                    other diagnostic data ("Usage Data").
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Tracking & Cookies Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We use cookies and similar tracking technologies to track
                    the activity on our Service and hold certain information.
                    Cookies are files with small amount of data which may
                    include an anonymous unique identifier. Cookies are sent to
                    your browser from a website and stored on your device.
                    Tracking technologies also used are beacons, tags, and
                    scripts to collect and track information and to improve and
                    analyze our Service. You can instruct your browser to refuse
                    all cookies or to indicate when a cookie is being sent.
                    However, if you do not accept cookies, you may not be able
                    to use some portions of our Service. Examples of Cookies we
                    use:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Session Cookies. We use Session Cookies to operate our
                    Service.
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Preference Cookies. We use Preference Cookies to remember
                    your preferences and various settings.
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·Security Cookies. We use Security Cookies for security
                    purposes.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Use of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Trendfactor Co. Ltd. uses the collected data for various
                    purposes:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide and maintain the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To notify you about changes to our Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To allow you to participate in interactive features of our
                    Service when you choose to do so
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide customer care and support
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To provide analysis or valuable information so that we can
                    improve the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To monitor the usage of the Service
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·To detect, prevent and address technical issues
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Transfer Of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Your information, including Personal Data, may be
                    transferred to — and maintained on — computers located
                    outside of your state, province, country or other
                    governmental jurisdiction where the data protection laws may
                    differ than those from your jurisdiction. If you are located
                    outside Thailand and choose to provide information to us,
                    please note that we transfer the data, including Personal
                    Data, to Thailand and process it there. Your consent to this
                    Privacy Policy followed by your submission of such
                    information represents your agreement to that transfer.
                    Trendfactor Co. Ltd. will take all steps reasonably
                    necessary to ensure that your data is treated securely and
                    in accordance with this Privacy Policy and no transfer of
                    your Personal Data will take place to an organization or a
                    country unless there are adequate controls in place
                    including the security of your data and other personal
                    information.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Security Of Data
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    The security of your data is important to us, but remember
                    that no method of transmission over the Internet, or method
                    of electronic storage is 100% secure. While we strive to use
                    commercially acceptable means to protect your Personal Data,
                    we cannot guarantee its absolute security.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Service Providers
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We may employ third party companies and individuals to
                    facilitate our Service ("Service Providers"), to provide the
                    Service on our behalf, to perform Service-related services
                    or to assist us in analyzing how our Service is used. These
                    third parties have access to your Personal Data only to
                    perform these tasks on our behalf and are obligated not to
                    disclose or use it for any other purpose.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Links To Other Sites
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Our Service may contain links to other sites that are not
                    operated by us. If you click on a third party link, you will
                    be directed to that third party's site. We strongly advise
                    you to review the Privacy Policy of every site you visit. We
                    have no control over and assume no responsibility for the
                    content, privacy policies or practices of any third party
                    sites or services.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Children's Privacy
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    Our Service does not address anyone under the age of 18
                    ("Children"). We do not knowingly collect personally
                    identifiable information from anyone under the age of 18. If
                    you are a parent or guardian and you are aware that your
                    Children has provided us with Personal Data, please contact
                    us. If we become aware that we have collected Personal Data
                    from children without verification of parental consent, we
                    take steps to remove that information from our servers.{" "}
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Changes To This Privacy Policy
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    We may update our Privacy Policy from time to time. We will
                    notify you of any changes by posting the new Privacy Policy
                    on this page. We will let you know via email and/or a
                    prominent notice on our Service, prior to the change
                    becoming effective and update the "effective date" at the
                    top of this Privacy Policy. You are advised to review this
                    Privacy Policy periodically for any changes. Changes to this
                    Privacy Policy are effective when they are posted on this
                    page.
                  </Text>
                  <Text
                    style={{
                      marginTop: 5,
                      textAlign: "left",
                      fontWeight: "bold",
                      marginLeft: 3
                    }}
                  >
                    Contact Us
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    If you have any questions about this Privacy Policy, please
                    contact us:
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·By email: help@trendfactor.co
                  </Text>
                  <Text style={{ textAlign: "left", marginLeft: 3 }}>
                    ·By visiting this page on our website: www.ddlion.me improve
                    the Service
                  </Text>
                </ScrollView>
              </View>
              <Button
                onPress={this.agreeHandle}
                title="Agree"
                color="#fff"
                fontSize={16}
                buttonStyle={{
                  marginTop: 20,
                  width: 120,
                  height: 40,
                  backgroundColor: "rgb(253,92,99)",
                  borderRadius: 4,
                  padding: 0
                }}
                accessibilityLabel="Learn more about this purple button"
              />
            </View>
          </View>
        )}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 48,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 50,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  mainContainer: {
    position: "relative",
    margin: 20,
    width: 200,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  intro: {
    paddingTop: 10,
    width,
    height: 60,
    backgroundColor: "#F5FCFF",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  introText: {
    marginLeft: 22,
    fontSize: 16,
    color: "rgb(155,155,155)"
  },
  itemContainer: {
    width,
    backgroundColor: "rgb(255,255,255)"
  },
  itemWrapper: {
    marginLeft: 22,
    width: width - 22,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  itemTitle: {
    fontSize: 20,
    marginTop: 8,
    marginBottom: 8
  },
  itemText: {
    marginBottom: 8,
    fontSize: 16,
    color: "rgb(155,155,155)"
  }
});
