// icons
const back = require("../../assets/icons/back.png");
const camera = require("../../assets/icons/camera.png");
const down_arrow = require("../../assets/icons/down_arrow.png");
const close = require("../../assets/icons/cancel-white.png");

// components
import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ActivityIndicator,
  View,
  TextInput
} from "react-native";
import Overlay from "../../components/custom/Overlay";
import ImagePicker from "react-native-image-crop-picker";
import { assetRoot } from "../../constants/urlConstant";
import { Button, ListItem } from "react-native-elements";
import Picker from "react-native-picker";
import { createReport, uploadImage } from "../../actions/report";
const { width, height } = Dimensions.get("window");

const restrictW = width - 40;
const imageWidth = (width - 35) / 4;

const pushNoti = () => (
  <View style={styles.formItem2}>
    <Text>Push-notifications</Text>
    <Switch
      value={true}
      onValueChange={() => {}}
      onTintColor="rgb(44,197,50)"
      thumbTintColor="#fff"
    />
  </View>
);
var fetchParams = {
  first: 6,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({
  styles,
  opacity,
  leftEvent,
  rightShow,
  rightEvent,
  title
}) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={back}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      {rightShow ? (
        <TouchableOpacity onPress={rightEvent}>
          <View style={styles.share}>
            <Text
              style={{
                color: "rgb(253,92,99)",
                fontSize: 14,
                fontWeight: "bold"
              }}
            >
              Submit
            </Text>
          </View>
        </TouchableOpacity>
      ) : (
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          />
        </View>
      )}
    </View>
  );
};

const Arrow = () => <Image source={down_arrow} />;
const ImageItem = ({ url, deleteImage }) => {
  return (
    <View style={styles.imageItem}>
      <View style={styles.imageSelected}>
        <TouchableOpacity onPress={this.deleteImage}>
          <View style={styles.dot}>
            <Image
              style={{ width: 25, height: 25, marginTop: -2 }}
              source={close}
            />
          </View>
        </TouchableOpacity>
      </View>
      <Image
        style={{ width: 85, height: 85, borderRadius: 4 }}
        source={{ uri: `${assetRoot}/${url}` }}
      />
    </View>
  );
};

export default class OtherReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tip: null,
      isLoading: false,
      selectedIndex: null,
      reasonText: "",
      currentImage: undefined,
      isFetching: false
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    var _that = this;
  }

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  selectReasonHandle = selectedIndex => {
    const { selectedIndex: preInex } = this.state;
    this.setState({
      selectedIndex: preInex === selectedIndex ? null : selectedIndex
    });
  };

  submitReaseon = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props;
    const { currentImage, reasonText } = this.state;
    const { navigation } = this.props;
    let idObj = {};
    if (params.postId) {
      idObj.postId = params.postId;
    } else if (params.userId) {
      idObj.userId = params.userId;
    }
    this.setState({
      isFetching: true
    });
    createReport(
      Object.assign(
        {},
        {
          reason: reasonText,
          screenshot: currentImage,
          other: reasonText
        },
        idObj
      )
    ).then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState({
          isFetching: false
        });
        navigation.goBack();
      }
    });
  };

  changeContent = event => {
    const reasonText = event.nativeEvent.text;
    this.setState({
      reasonText
    });
  };

  wakeUpCamera = e => {
    ImagePicker.openPicker({
      width: 755,
      height: 755,
      cropping: true
    })
      .then(image => {
        this.setState({
          isFetching: true
        });
        return uploadImage([image.path]);
      })
      .then(res => {
        const { code, data } = res;
        if (code === 200) {
          const currentImage = data[0];
          this.setState({
            currentImage,
            isFetching: false
          });
        }
      });
  };

  deleteImage = () => {
    this.setState({
      currentImage: undefined
    });
  };

  render() {
    const { tip, isLoading, reasonText, currentImage, isFetching } = this.state;
    const enableSubmit = reasonText.length >= 50;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Other"}
          leftEvent={this.goBack}
          rightShow={enableSubmit}
          rightEvent={this.submitReaseon}
        />
        <View style={styles.wrapper}>
          <View style={styles.intro}>
            <Text style={styles.introText}>
              please write down your feedback below.
            </Text>
          </View>
          <View style={styles.textContainer}>
            <TextInput
              ref="postContent"
              multiline={true}
              style={styles.textarea}
              maxLength={500}
              value={reasonText}
              onChange={this.changeContent}
              onEndEditing={this.changeContent}
            />
          </View>
          <View style={{ marginTop: 10 }}>
            <Text style={styles.introText}>Screenshot (optional)</Text>
            <View style={{ marginTop: 10, marginLeft: 22 }}>
              {currentImage ? (
                <ImageItem url={currentImage} deleteImage={this.deleteImage} />
              ) : (
                <TouchableOpacity
                  style={{ width: imageWidth, height: imageWidth }}
                  onPress={this.wakeUpCamera}
                >
                  <View style={styles.openCamera}>
                    <Image style={{ width: 20, height: 17 }} source={camera} />
                  </View>
                </TouchableOpacity>
              )}
            </View>
          </View>
        </View>
        {isFetching && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 48,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 50,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  mainContainer: {
    position: "relative",
    margin: 20,
    width: 200,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  intro: {
    paddingTop: 10,
    width,
    height: 40,
    backgroundColor: "#F5FCFF",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  introText: {
    marginLeft: 22,
    fontSize: 16,
    color: "rgb(155,155,155)"
  },
  textContainer: {
    width,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(255,255,255)",
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  textarea: {
    width: width - 44,
    height: 200,
    alignSelf: "center",
    overflow: "hidden",
    fontSize: 16
  },
  openCamera: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: 2.5,
    borderRadius: 4,
    width: imageWidth,
    height: imageWidth,
    backgroundColor: "rgb(253,92,99)"
  },
  imageSelected: {
    position: "absolute",
    zIndex: 4,
    top: 0,
    left: 0,
    borderRadius: 4,
    width: imageWidth,
    height: imageWidth,
    backgroundColor: "rgba(253,92,99,0.5)",
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    width: 30,
    height: 30,
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  }
});
