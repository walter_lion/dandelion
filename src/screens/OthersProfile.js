import React, { Component } from "react";
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TouchableWithoutFeedback,
  ActionSheetIOS,
  Alert
} from "react-native";
import moment from "moment";
import {
  fetchUserInfo,
  fetchLikedPosts,
  fetchIndividalPosts
} from "../actions/profile";
import {
  unFollowUser,
  followingUser,
  blockUser,
  unBlockUser
} from "../actions/user";
import { assetRoot } from "../constants/urlConstant";
import { Avatar, Button } from "react-native-elements";
import PostItem from "../components/PostItem/PostItem";
import LikePostItem from "../components/PostItem/LikePostItem";
const { width, height } = Dimensions.get("window");
const restrictW = width - 40;
const postItemWidth = (width - 27) / 2;
const postItemWidth_sin = width - 20;
const postItemHeight = (postItemWidth * 200) / 174;
const postItemHeight_sin = (postItemWidth_sin * 200) / 174;

const numShow = num => {
  if (num / 1000 >= 1) {
    return `${num / 1000}K+`;
  } else {
    return num;
  }
};
const Header = ({ styles, leftEvent, rightEvent }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={require("../assets/icons/cancel.png")}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>Profile</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={require("../assets/icons/more.png")}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default class OthersProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      isFetching: false,
      isFetched: false,
      tabType: "POST",
      postList: {},
      userInfo: {},
      layout: 2
    };
  }
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  chooseGrid = () => {
    this.setState({
      layout: 2
    });
  };

  chooseLine = () => {
    this.setState({
      layout: 1
    });
  };

  switchTab = tabType => {
    this.setState({
      tabType
    });
  };

  componentDidMount() {
    this.fetInitData().then(res => {
      if (res === "success") {
        this.setState({
          refreshing: false
        });
      }
    });
  }

  fetInitData = () => {
    const {
      state: { params }
    } = this.props.navigation;
    return fetchUserInfo(params.userId)
      .then(res => {
        const { code, data } = res;
        if (code === 200) {
          this.setState({
            isFollowed: data.isFollowed,
            isBlocked: data.isBlocked,
            userInfo: data.user
          });
        }
        return Promise.resolve(data.user.userId);
      })
      .then(userId => {
        return fetchIndividalPosts(userId, 0)
          .then(res => {
            const { code, data } = res;
            if (code === 200) {
              let postList = {
                POST: {
                  pagination: {
                    totalPages: data.totalPages,
                    last: data.last,
                    currentPage: data.number
                  },
                  content: data.content
                }
              };
              this.setState({
                postList
              });
              return Promise.resolve(userId);
            }
          })
          .then(userId => {
            return fetchLikedPosts(userId, 0).then(res => {
              const { code, data } = res;
              if (code === 200) {
                let likePosts = {
                  pagination: {
                    totalPages: data.totalPages,
                    last: data.last,
                    currentPage: data.number
                  },
                  content: data.content
                };
                let postList = this.state.postList;
                postList["LIKE"] = likePosts;
                this.setState({ postList });
                return Promise.resolve("success");
              }
            });
          });
      });
  };
  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetInitData().then(res => {
      if (res === "success") {
        this.setState({ refreshing: false });
      }
    });
  };
  onScrollHandle = event => {
    const { isFetching, refreshing, tabType, postList } = this.state;
    const last =
      (postList[tabType] && postList[tabType].pagination.last) || false;
    if (isFetching || refreshing || last) {
      return;
    }
    let y = event.nativeEvent.contentOffset.y;
    let height = event.nativeEvent.layoutMeasurement.height;
    let contentHeight = event.nativeEvent.contentSize.height;
    if (y + height >= contentHeight - 20) {
      this.onLoaderMore();
    }
  };
  onLoaderMore = () => {
    const {
      tabType,
      userInfo: { userId },
      postList
    } = this.state;
    let { POST, LIKE } = postList;
    if (tabType === "POST") {
      const pagination = POST.pagination;
      if (!pagination.last) {
        this.setState({
          isFetching: true
        });
        fetchIndividalPosts(userId, pagination.currentPage + 1).then(res => {
          const { code, data } = res;
          if (code === 200) {
            POST.pagination.currentPage = data.number;
            POST.pagination.last = data.last;
            POST.content = POST.content.concat(data.content);
            this.setState({
              postList,
              isFetching: false
            });
          }
        });
      }
    } else {
      const pagination = LIKE.pagination;
      if (!pagination.last) {
        this.setState({
          isFetching: true
        });
        fetchLikedPosts(userId, pagination.currentPage + 1).then(res => {
          const { code, data } = res;
          if (code === 200) {
            LIKE.pagination.currentPage = data.number;
            LIKE.pagination.last = data.last;
            LIKE.content = LIKE.content.concat(data.content);
            this.setState({
              postList,
              isFetching: false
            });
          }
        });
      }
    }
  };

  toggleFollow = () => {
    let {
      isFollowed,
      userInfo: { userId, following, followers }
    } = this.state;
    if (isFollowed !== 1) {
      isFollowed = 1;
      followers = followers + 1;
      followingUser(userId);
    } else {
      isFollowed = 0;
      followers = followers - 1;
      unFollowUser(userId);
    }
    this.setState({
      isFollowed,
      userInfo: {
        ...this.state.userInfo,
        followers
      }
    });
  };

  unBlockHand = () => {
    blockUser;
    let {
      isBlocked,
      userInfo: { userId }
    } = this.state;
    if (isBlocked === 1) {
      isBlocked = 0;
      unBlockUser(userId);
    }
    this.setState({
      isBlocked
    });
  };

  toggleMore = () => {
    const { navigation } = this.props;
    const {
      userInfo: { userId }
    } = this.state;
    // navigation.navigate("Report");
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ["Block", "Report", "Cancel"],
        destructiveButtonIndex: 0,
        cancelButtonIndex: 2
      },
      buttonIndex => {
        if (buttonIndex === 0) {
          /* destructive action */
          Alert.alert(
            "", //提示标题
            `Are you sure to block this user? `, //提示内容
            [
              {
                text: "Cancel",
                onPress: () => console.log("Cancel Pressed"),
                style: "cancel"
              },
              {
                text: "Block",
                onPress: () => {
                  blockUser(userId);
                }
              }
            ]
          );
        } else if (buttonIndex === 1) {
          navigation.navigate("Report", {
            userId
          });
        }
      }
    );
  };

  render() {
    const {
      refreshing,
      postList,
      isBlocked,
      isFollowed,
      userInfo,
      tabType,
      layout
    } = this.state;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          leftEvent={this.goBack}
          rightEvent={this.toggleMore}
        />
        {/*<ActivityIndicator animating={true} color="gray" size="large" />*/}
        <ScrollView
          onScroll={this.onScrollHandle}
          scrollEventThrottle={10}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View style={styles.infoWrapper}>
            <View style={styles.profile}>
              <Avatar
                width={60}
                height={60}
                rounded
                source={{ uri: `${assetRoot}/${userInfo.avatarPhoto}` }}
                onPress={() => console.log("Works!")}
                activeOpacity={0.7}
              />
              <View style={styles.infoDetail}>
                <View style={styles.infoHead}>
                  <View>
                    <Text
                      style={{
                        fontSize: 16,
                        color: "rgb(51,51,51)",
                        fontWeight: "bold"
                      }}
                    >
                      {userInfo.name || ""}
                    </Text>
                  </View>
                  {isBlocked !== 1 &&
                    isFollowed !== -1 && (
                      <Button
                        onPress={this.toggleFollow}
                        title={isFollowed === 0 ? "+ Follow" : "Unfollow"}
                        textStyle={{
                          width: 62,
                          height: 22,
                          fontSize: 11,
                          color: "#fff",
                          textAlign: "center",
                          lineHeight: 22
                        }}
                        buttonStyle={styles.followBtn}
                      />
                    )}
                  {isBlocked === 1 && (
                    <Button
                      onPress={this.unBlockHand}
                      title="Unblock"
                      textStyle={{
                        width: 62,
                        height: 22,
                        fontSize: 11,
                        color: "rgb(253,92,99)",
                        textAlign: "center",
                        lineHeight: 22
                      }}
                      buttonStyle={styles.blockBtn}
                    />
                  )}
                </View>
                <View style={{ marginTop: 10 }}>
                  <Text style={{ color: "rgb(155,155,155)", fontSize: 14 }}>
                    {`${
                      userInfo.gender !== null && userInfo.gender
                        ? userInfo.gender
                        : ""
                    }  ${
                      userInfo.birthday !== null && userInfo.birthday
                        ? "/ " + moment(userInfo.birthday).format("ll")
                        : ""
                    }  ${
                      userInfo.location !== null && userInfo.location
                        ? "/ " + userInfo.location
                        : ""
                    }`}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.dataInfo}>
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.posts)}</Text>
                <Text style={styles.desc}>Posts</Text>
              </View>
              <View
                style={{
                  width: 1,
                  height: 10,
                  backgroundColor: "rgb(155,155,155)"
                }}
              />
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.likes)}</Text>
                <Text style={styles.desc}>Likes</Text>
              </View>
              <View
                style={{
                  width: 1,
                  height: 10,
                  backgroundColor: "rgb(155,155,155)"
                }}
              />
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.followers)}</Text>
                <Text style={styles.desc}>Followers</Text>
              </View>
              <View
                style={{
                  width: 1,
                  height: 10,
                  backgroundColor: "rgb(155,155,155)"
                }}
              />
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.following)}</Text>
                <Text style={styles.desc}>Following</Text>
              </View>
            </View>
          </View>
          <View style={styles.wrapper}>
            <View style={styles.controlBar}>
              <View style={styles.tabBars}>
                <TouchableWithoutFeedback
                  onPress={this.switchTab.bind(null, "POST")}
                >
                  <View style={{ width: 53, height: 19 }}>
                    <Text
                      style={
                        tabType === "POST"
                          ? styles.tabBarsText
                          : styles.tabBarsTextAc
                      }
                    >
                      Posts
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <View
                  style={{
                    height: 19,
                    width: 1,
                    backgroundColor: "rgb(211,211,211)"
                  }}
                />
                <TouchableWithoutFeedback
                  onPress={this.switchTab.bind(null, "LIKE")}
                >
                  <View style={{ marginLeft: 10, width: 77, height: 19 }}>
                    <Text
                      style={
                        tabType === "LIKE"
                          ? styles.tabBarsText
                          : styles.tabBarsTextAc
                      }
                    >
                      Likes
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{ ...styles.tabBars, justifyContent: "flex-end" }}>
                <View style={{ width: 15, height: 15, marginRight: 15 }}>
                  <TouchableWithoutFeedback onPress={this.chooseGrid}>
                    <Image
                      source={
                        layout === 2
                          ? require("../assets/icons/grid_selected.png")
                          : require("../assets/icons/grid.png")
                      }
                      style={{ width: 15, height: 15 }}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={{ width: 15, height: 15 }}>
                  <TouchableWithoutFeedback onPress={this.chooseLine}>
                    <Image
                      source={
                        layout === 1
                          ? require("../assets/icons/tile_selected.png")
                          : require("../assets/icons/tile.png")
                      }
                      style={{ width: 15, height: 15 }}
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </View>
            <View style={styles.flatView}>
              {tabType === "POST" ? (
                <FlatList
                  data={(postList.POST && postList.POST.content) || []}
                  key={layout}
                  keyExtractor={(item, index) => item.postId}
                  numColumns={layout}
                  renderItem={({ item }) => (
                    <LikePostItem
                      navigation={this.props.navigation}
                      id={item.postId}
                      postPhotoUrl={item.url}
                      postTitle={item.title}
                      width={layout === 2 ? postItemWidth : postItemWidth_sin}
                      height={
                        layout === 2 ? postItemHeight : postItemHeight_sin
                      }
                    />
                  )}
                />
              ) : (
                <FlatList
                  data={(postList.LIKE && postList.LIKE.content) || []}
                  key={layout}
                  keyExtractor={(item, index) => item.postId}
                  numColumns={layout}
                  renderItem={({ item }) => (
                    <LikePostItem
                      navigation={this.props.navigation}
                      id={item.postId}
                      postPhotoUrl={item.url}
                      postTitle={item.title}
                      width={layout === 2 ? postItemWidth : postItemWidth_sin}
                      height={
                        layout === 2 ? postItemHeight : postItemHeight_sin
                      }
                    />
                  )}
                />
              )}
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 57,
    width: 18,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 57,
    width: restrictW - 36,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 57,
    width: 18,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(251,251,252)",
    width,
    height
  },
  infoWrapper: {
    marginTop: 10,
    display: "flex",
    alignItems: "center",
    width,
    height: 140
  },
  profile: {
    position: "relative",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width: restrictW,
    height: 60
  },
  infoDetail: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    width: restrictW - 70,
    marginLeft: 10,
    height: 60
  },
  infoHead: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  followBtn: {
    width: 57,
    height: 22,
    borderRadius: 4,
    backgroundColor: "rgb(253,92,99)"
  },
  blockBtn: {
    width: 57,
    height: 22,
    borderRadius: 4,
    borderWidth: 1,
    borderColor: "rgb(253,92,99)",
    backgroundColor: "rgb(255,255,255)"
  },
  value: {
    height: 24,
    fontSize: 20,
    textAlign: "center",
    color: "black",
    fontWeight: "bold"
  },
  desc: {
    height: 15,
    fontSize: 12,
    textAlign: "center",
    color: "rgb(155,155,155)"
  },
  mask: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    bottom: 0,
    width: 230,
    height: 40,
    opacity: 0.8
  },
  maskText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 14
  },
  dataInfo: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 16,
    width: restrictW,
    height: 60
  },
  itemBox: {
    width: restrictW / 4,
    height: 40
  },
  wrapper: {
    width,
    // height: 300,
    backgroundColor: "rgb(251,251,252)"
  },
  tabBars: {
    display: "flex",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    width: restrictW / 2,
    height: 50
  },
  tabBarsText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "rgb(51,51,51)"
  },
  tabBarsTextAc: {
    fontSize: 16,
    fontWeight: "bold",
    color: "rgb(211,211,211)"
  },
  flatView: {
    display: "flex",
    justifyContent: "center",
    padding: 11,
    width,
    marginBottom: 50
  },
  controlBar: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width,
    height: 50
  }
});
