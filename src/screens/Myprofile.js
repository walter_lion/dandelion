const mail = require("../assets/icons/mail.png");
const setting = require("../assets/icons/setting.png");
import { assetRoot } from "../constants/urlConstant";
import MyPostItem from "../components/PostItem/MyPostItem";
import LikePostItem from "../components/PostItem/LikePostItem";
import React, { Component } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  RefreshControl,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  Alert,
  ActivityIndicator
} from "react-native";
import LinearGradient from "react-native-linear-gradient";
import {
  fetchUserInfo,
  fetchLikedPosts,
  fetchIndividalPosts
} from "../actions/profile";
import { deletePost } from "../actions/post";
const { width, height } = Dimensions.get("window");
const restrictW = width - 40;
const postItemWidth = (width - 27) / 2;
const postItemWidth_sin = width - 20;
const postItemHeight = (postItemWidth * 200) / 174;
const postItemHeight_sin = (postItemWidth_sin * 200) / 174;

const avatarSize = width * 0.6 >= 230 ? 230 : 210;
const numShow = num => {
  if (num / 1000 >= 1) {
    return `${num / 1000}K+`;
  } else {
    return num;
  }
};
const Header = ({ username, styles, setEvent, goNotification }) => {
  return (
    <View style={styles.header}>
      <View style={styles.myName}>
        <Text style={styles.myNameText}>{username}</Text>
      </View>
      <View style={styles.buttons}>
        <TouchableOpacity onPress={goNotification}>
          <Image source={mail} style={{ width: 32, height: 32 }} />
        </TouchableOpacity>
        <TouchableOpacity onPress={setEvent}>
          <Image source={setting} style={{ width: 32, height: 32 }} />
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default class Myprofile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      isFetching: false,
      isFetched: false,
      tabType: "MY_POST",
      postList: {},
      userInfo: {},
      layout: 2
    };
  }

  componentDidMount() {
    this.fetInitData().then(res => {
      if (res === "success") {
        this.setState({
          refreshing: false
        });
      }
    });
  }

  fetInitData = () => {
    return fetchUserInfo()
      .then(res => {
        const { code, data } = res;
        if (code === 200) {
          this.setState({
            userInfo: data.user
          });
        }
        return Promise.resolve(data.user.userId);
      })
      .then(userId => {
        return fetchIndividalPosts(userId, 0)
          .then(res => {
            const { code, data } = res;
            if (code === 200) {
              let postList = {
                MY_POST: {
                  pagination: {
                    totalPages: data.totalPages,
                    last: data.last,
                    currentPage: data.number
                  },
                  content: data.content
                }
              };
              this.setState({
                postList
              });
              return Promise.resolve(userId);
            }
          })
          .then(userId => {
            return fetchLikedPosts(userId, 0).then(res => {
              const { code, data } = res;
              if (code === 200) {
                let likePosts = {
                  pagination: {
                    totalPages: data.totalPages,
                    last: data.last,
                    currentPage: data.number
                  },
                  content: data.content
                };
                let postList = this.state.postList;
                postList["MY_LIKES"] = likePosts;
                this.setState({ postList });
                return Promise.resolve("success");
              }
            });
          });
      });
  };
  EditProfile = () => {
    const { navigation } = this.props;
    navigation.navigate("EditProfile", {
      userInfo: this.state.userInfo,
      refreshHandle: this.fetInitData
    });
  };
  goSetting = () => {
    const { navigation } = this.props;
    navigation.navigate("Setting");
  };

  goNotification = () => {
    const { navigation } = this.props;
    navigation.navigate("Notifications");
  };

  chooseGrid = () => {
    this.setState({
      layout: 2
    });
  };

  chooseLine = () => {
    this.setState({
      layout: 1
    });
  };

  switchTab = tabType => {
    this.setState({
      tabType
    });
  };
  onScrollHandle = event => {
    const { isFetching, refreshing, tabType, postList } = this.state;
    const last =
      (postList[tabType] && postList[tabType].pagination.last) || false;
    if (isFetching || refreshing || last) {
      return;
    }
    let y = event.nativeEvent.contentOffset.y;
    let height = event.nativeEvent.layoutMeasurement.height;
    let contentHeight = event.nativeEvent.contentSize.height;

    if (y + height >= contentHeight - 20) {
      this.onLoaderMore();
    }
  };

  onLoaderMore = () => {
    const {
      tabType,
      userInfo: { userId },
      postList
    } = this.state;
    let { MY_POST, MY_LIKES } = postList;
    if (tabType === "MY_POST") {
      const pagination = MY_POST.pagination;
      if (!pagination.last) {
        this.setState({
          isFetching: true
        });
        fetchIndividalPosts(userId, pagination.currentPage + 1).then(res => {
          const { code, data } = res;
          if (code === 200) {
            MY_POST.pagination.currentPage = data.number;
            MY_POST.pagination.last = data.last;
            MY_POST.content = MY_POST.content.concat(data.content);
            this.setState({
              postList,
              isFetching: false
            });
          }
        });
      }
    } else {
      const pagination = MY_LIKES.pagination;
      if (!pagination.last) {
        this.setState({
          isFetching: true
        });
        fetchLikedPosts(userId, pagination.currentPage + 1).then(res => {
          const { code, data } = res;
          if (code === 200) {
            MY_LIKES.pagination.currentPage = data.number;
            MY_LIKES.pagination.last = data.last;
            MY_LIKES.content = MY_LIKES.content.concat(data.content);
            this.setState({
              postList,
              isFetching: false
            });
          }
        });
      }
    }
  };

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.fetInitData().then(res => {
      if (res === "success") {
        this.setState({ refreshing: false });
      }
    });
  };

  deletePostHandle = postId => {
    const { postList } = this.state;
    let { MY_POST } = postList;
    deletePost(postId);
    MY_POST.content = MY_POST.content.filter(post => post.postId !== postId);
    this.setState({
      postList
    });
  };
  render() {
    let {
      refreshing,
      isFetching,
      userInfo,
      layout,
      tabType,
      postList
    } = this.state;
    const { MY_POST, MY_LIKES } = postList;
    const last =
      (postList[tabType] && postList[tabType].pagination.last) || false;
    return (
      <View style={styles.container}>
        <ScrollView
          onScroll={this.onScrollHandle}
          scrollEventThrottle={10}
          refreshControl={
            <RefreshControl
              refreshing={refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <Header
            username={userInfo.name}
            styles={headerStyles}
            setEvent={this.goSetting}
            goNotification={this.goNotification}
          />
          <View style={styles.infoWrapper}>
            <TouchableWithoutFeedback onPress={this.EditProfile}>
              <View
                style={{
                  ...styles.profile,
                  width: avatarSize,
                  height: avatarSize
                }}
              >
                <Image
                  style={{ top: 0, width: avatarSize, height: avatarSize }}
                  source={{ uri: `${assetRoot}/${userInfo.avatarPhoto}` }}
                />
                <LinearGradient
                  colors={["rgba(0,0,0,0.2)", "rgba(0,0,0,1)"]}
                  style={{
                    ...styles.mask,
                    width: avatarSize
                  }}
                >
                  <Text style={styles.maskText}>Edit</Text>
                </LinearGradient>
              </View>
            </TouchableWithoutFeedback>
            <View
              style={{
                ...styles.dataInfo,
                width: width * 0.3,
                height: avatarSize
              }}
            >
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.posts)}</Text>
                <Text style={styles.desc}>Posts</Text>
              </View>
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.likes)}</Text>
                <Text style={styles.desc}>Be Liked</Text>
              </View>
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.followers)}</Text>
                <Text style={styles.desc}>Followers</Text>
              </View>
              <View style={styles.itemBox}>
                <Text style={styles.value}>{numShow(userInfo.following)}</Text>
                <Text style={styles.desc}>Following</Text>
              </View>
            </View>
          </View>
          <View style={styles.wrapper}>
            <View style={styles.controlBar}>
              <View style={styles.tabBars}>
                <TouchableWithoutFeedback
                  onPress={this.switchTab.bind(null, "MY_POST")}
                >
                  <View style={{ width: 77, height: 19 }}>
                    <Text
                      style={
                        tabType === "MY_POST"
                          ? styles.tabBarsText
                          : styles.tabBarsTextAc
                      }
                    >
                      My Posts
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
                <View
                  style={{
                    height: 19,
                    width: 1,
                    backgroundColor: "rgb(211,211,211)"
                  }}
                />
                <TouchableWithoutFeedback
                  onPress={this.switchTab.bind(null, "MY_LIKES")}
                >
                  <View style={{ marginLeft: 10, width: 77, height: 19 }}>
                    <Text
                      style={
                        tabType === "MY_LIKES"
                          ? styles.tabBarsText
                          : styles.tabBarsTextAc
                      }
                    >
                      My Likes
                    </Text>
                  </View>
                </TouchableWithoutFeedback>
              </View>
              <View style={{ ...styles.tabBars, justifyContent: "flex-end" }}>
                <View style={{ width: 15, height: 15, marginRight: 15 }}>
                  <TouchableWithoutFeedback onPress={this.chooseGrid}>
                    <Image
                      source={
                        layout === 2
                          ? require("../assets/icons/grid_selected.png")
                          : require("../assets/icons/grid.png")
                      }
                      style={{ width: 15, height: 15 }}
                    />
                  </TouchableWithoutFeedback>
                </View>
                <View style={{ width: 15, height: 15 }}>
                  <TouchableWithoutFeedback onPress={this.chooseLine}>
                    <Image
                      source={
                        layout === 1
                          ? require("../assets/icons/tile_selected.png")
                          : require("../assets/icons/tile.png")
                      }
                      style={{ width: 15, height: 15 }}
                    />
                  </TouchableWithoutFeedback>
                </View>
              </View>
            </View>
            <View style={styles.flatView}>
              {tabType === "MY_POST" ? (
                <FlatList
                  key={layout}
                  data={
                    (MY_POST &&
                      MY_POST.content.filter(item => item !== null)) || [
                      { postId: 1 }
                    ]
                  }
                  keyExtractor={(item, index) => item.postId}
                  numColumns={layout}
                  renderItem={({ item }) => (
                    <MyPostItem
                      navigation={this.props.navigation}
                      id={item.postId}
                      postPhotoUrl={item.url}
                      postTitle={item.title}
                      deletePost={this.deletePostHandle}
                      width={layout === 2 ? postItemWidth : postItemWidth_sin}
                      height={
                        layout === 2 ? postItemHeight : postItemHeight_sin
                      }
                    />
                  )}
                />
              ) : (
                <FlatList
                  key={layout}
                  data={
                    (MY_LIKES &&
                      MY_LIKES.content.filter(item => item !== null)) || [
                      { postId: 1 }
                    ]
                  }
                  keyExtractor={(item, index) => item.postId}
                  numColumns={layout}
                  renderItem={({ item }) => (
                    <LikePostItem
                      navigation={this.props.navigation}
                      id={item.postId}
                      postPhotoUrl={item.url}
                      postTitle={item.title}
                      width={layout === 2 ? postItemWidth : postItemWidth_sin}
                      height={
                        layout === 2 ? postItemHeight : postItemHeight_sin
                      }
                    />
                  )}
                />
              )}
            </View>
          </View>
          {!last && isFetching && <ActivityIndicator />}
          {last && (
            <View style={styles.noMoreTip}>
              <Text style={styles.noMoreTipText}>No more</Text>
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    marginBottom: 1
  },
  myName: {
    display: "flex",
    marginTop: 44,
    marginLeft: 20,
    height: 42,
    justifyContent: "flex-start"
  },
  myNameText: {
    lineHeight: 42,
    fontWeight: "bold",
    fontSize: 21,
    color: "black",
    textAlign: "left"
  },
  buttons: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 50,
    marginRight: 20,
    width: 83,
    height: 42
  }
});

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(251,251,252)",
    width,
    height
  },
  infoWrapper: {
    position: "relative",
    display: "flex",
    alignSelf: "center",
    flexDirection: "row",
    justifyContent: "flex-start",
    width: width - 32,
    height: 230
  },
  profile: {
    position: "relative",
    backgroundColor: "rgba(0,0,0,0.2)"
  },
  value: {
    width: 61,
    height: 24,
    fontSize: 20,
    color: "black"
  },
  desc: {
    width: 61,
    height: 15,
    fontSize: 12,
    color: "rgb(155,155,155)"
  },
  mask: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    bottom: 0,
    // width: 230,
    height: 40,
    opacity: 0.8
  },
  maskText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 14,
    lineHeight: 40
  },
  dataInfo: {
    paddingLeft: 16,
    marginRight: restrictW - 340,
    height: 230
  },
  itemBox: {
    width: 70,
    height: 40,
    marginTop: 14
  },
  wrapper: {
    display: "flex",
    alignItems: "center",
    width,
    backgroundColor: "rgb(251,251,252)"
  },
  tabBars: {
    display: "flex",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    width: width * 0.3,
    height: 50
  },
  tabBarsText: {
    fontSize: 16,
    fontWeight: "bold",
    color: "rgb(51,51,51)"
  },
  tabBarsTextAc: {
    fontSize: 16,
    fontWeight: "bold",
    color: "rgb(211,211,211)"
  },
  flatView: {
    display: "flex",
    justifyContent: "center",
    padding: 11,
    width,
    marginBottom: 120
  },
  controlBar: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: width - 32,
    height: 50
  },
  noMoreTip: {
    width,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  noMoreTipText: {
    color: "rgb(155,155,155)",
    fontSize: 14
  }
});
