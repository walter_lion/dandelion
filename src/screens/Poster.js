const close = require("../assets/icons/cancel-white.png");
const redlike = require("../assets/icons/redlikeo.png");
const myLike = require("../assets/icons/redlikei.png");
const share = require("../assets/icons/share.png");

import PostItem from "../components/PostItem/PostItem";
import React, { Component } from "react";
import {
  ActivityIndicator,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Alert,
  ActionSheetIOS
} from "react-native";
import moment from "moment";
import { Avatar, ButtonGroup } from "react-native-elements";
import Overlay from "../components/custom/Overlay";
import Swiper from "react-native-swiper";
import { assetRoot } from "../constants/urlConstant";
import { fetchPostDetail, unLikePostById, likePostById } from "../actions/post";
import { unFollowUser, followingUser } from "../actions/user";
const { width, height } = Dimensions.get("window");
const restrictW = width - 40;

const Header = ({ styles, opacity, leftEvent, rightEvent, title, desc }) => {
  return (
    <View
      style={{
        ...styles.header,
        borderBottomWidth: opacity == 1 ? 1 : 0,
        backgroundColor: `rgba(255,255,255,${opacity})`
      }}
    >
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 25, height: 25, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={{ ...styles.titleText, opacity: 1 / opacity }}>
          {title}
        </Text>
        <Image
          source={require("../assets/icons/dandelion_logo.png")}
          style={{
            ...styles.titleText,
            width: 140,
            height: 30,
            opacity: opacity
          }}
          resizeMode="contain"
        />
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={require("../assets/icons/more.png")}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};
export default class Poster extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isFetching: false,
      photos: [],
      isFollowed: false,
      isLiked: false,
      postData: null,
      userAvatarPhoto: null,
      userName: undefined,
      tags: [],
      headerOpacity: 0,
      posterImageIndex: 0
    };
  }
  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  handleSwiper = index => {
    this.setState({
      posterImageIndex: index
    });
  };

  componentDidMount() {
    const {
      navigation: {
        state: { params }
      }
    } = this.props;
    this.setState({
      isFetching: true
    });
    fetchPostDetail(params.postId).then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState({
          photos: data.photos,
          isFollowed: data.isFollowed,
          isLiked: data.isLiked === 1 || data.isLiked === -1,
          postData: data.post,
          tags: data.tags,
          userAvatarPhoto: data.userAvatarPhoto,
          userName: data.userName,
          isFetching: false
        });
      }
    });
  }

  scrollHandle = event => {
    const y = event.nativeEvent.contentOffset.y; //垂直滚动距离
    if (y > 200) {
      this.setState({
        headerOpacity: 1
      });
    }

    if (y <= 200) {
      this.setState({
        headerOpacity: y * 0.01
      });
    }
  };

  toggleFollow = () => {
    let { isFollowed, postData } = this.state;
    if (isFollowed === 1) {
      unFollowUser(postData.userId);
      isFollowed = 0;
    } else {
      isFollowed = 1;
      followingUser(postData.userId);
    }
    this.setState({
      isFollowed
    });
  };

  toggleLikePost = () => {
    const {
      navigation: {
        state: { params }
      }
    } = this.props;
    let { isLiked, postData } = this.state;
    if (isLiked) {
      params.unLikePost && params.unLikePost(postData.postId);
      unLikePostById(postData.postId);
    } else {
      params.likePost && params.likePost(postData.postId);
      likePostById(postData.postId);
    }
    this.setState({
      isLiked: !isLiked,
      postData: {
        ...postData,
        likes: !isLiked ? postData.likes + 1 : postData.likes - 1
      }
    });
  };

  goToWriter = () => {
    const {
      postData: { userId }
    } = this.state;
    const { navigation } = this.props;
    navigation.navigate("OthersProfile", {
      userId
    });
  };

  goToReport = () => {
    const { navigation } = this.props;
    const {
      postData: { postId }
    } = this.state;
    ActionSheetIOS.showActionSheetWithOptions(
      {
        options: ["Report", "Cancel"],
        cancelButtonIndex: 1
      },
      buttonIndex => {
        if (buttonIndex === 0) {
          navigation.navigate("Report", {
            postId
          });
        }
      }
    );
  };

  render() {
    const {
      posterImageIndex,
      headerOpacity,
      photos,
      isFollowed,
      isLiked,
      postData,
      tags,
      userAvatarPhoto,
      userName,
      isFetching
    } = this.state;

    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          opacity={headerOpacity}
          title={`${posterImageIndex + 1} / ${photos.length}`}
          desc={postData !== null ? postData.title : ""}
          leftEvent={this.goBack}
          rightEvent={this.goToReport}
        />
        <ScrollView onScroll={this.scrollHandle} scrollEventThrottle={30}>
          <View style={styles.posterImages}>
            <Swiper
              index={0}
              key={photos.length}
              showsPagination={false}
              onIndexChanged={this.handleSwiper}
              loop={false}
            >
              {photos.map((item, index) => (
                <View style={styles.slide}>
                  <Image
                    style={styles.posterImages}
                    source={{ uri: `${assetRoot}/${item.url}` }}
                    resizeMode="cover"
                  />
                </View>
              ))}
            </Swiper>
          </View>
          <View style={styles.wrapper}>
            {postData !== null ? (
              <View style={styles.flatView}>
                <View style={styles.postTitle}>
                  <Text
                    style={{
                      color: "rgb(51,51,51)",
                      fontSize: 18,
                      fontWeight: "bold"
                    }}
                  >
                    {postData.title}
                  </Text>
                </View>
                <View style={styles.postTitle}>
                  <Text
                    style={{
                      color: "rgb(51,51,51)",
                      fontSize: 16
                    }}
                  >
                    {postData.content}
                  </Text>
                </View>
                <View style={styles.tagsWrapper}>
                  {tags.map(item => (
                    <View style={styles.tagContainer}>
                      <Text style={styles.tagLabel}>#</Text>
                      <Text style={{ fontSize: 12, marginLeft: 6 }}>
                        {item.tagName}
                      </Text>
                    </View>
                  ))}
                </View>
              </View>
            ) : (
              <View />
            )}
          </View>
        </ScrollView>
        <View style={styles.controlBar}>
          <View style={styles.profileBox}>
            <TouchableOpacity onPress={this.goToWriter}>
              <Avatar
                small
                rounded
                source={{
                  uri: `${assetRoot}/${userAvatarPhoto}`
                }}
                activeOpacity={0.7}
              />
            </TouchableOpacity>

            <View>
              <View style={styles.profileDetail}>
                <TouchableOpacity onPress={this.goToWriter}>
                  <Text style={styles.profileDetailName}>
                    {userName ? userName : " "}
                  </Text>
                </TouchableOpacity>
                {isFollowed !== -1 && (
                  <TouchableOpacity onPress={this.toggleFollow}>
                    <Text style={styles.profileDetailFollow}>
                      {isFollowed === 0 ? "Follow" : "Unfollow"}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
              <Text style={styles.postTime}>
                {postData !== null && moment(postData.createTime).fromNow()}
              </Text>
            </View>
          </View>
          <View style={styles.thumbs}>
            <TouchableOpacity onPress={this.toggleLikePost}>
              <View style={styles.thumbesBox}>
                <Image
                  source={isLiked ? myLike : redlike}
                  style={styles.thumbsImage}
                />
                <Text style={styles.thumbsText}>
                  {postData !== null ? postData.likes : 0}
                </Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        {isFetching && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    position: "absolute",
    zIndex: 10,
    top: 0,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width,
    height: 88,
    borderBottomWidth: 0,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 57,
    width: 18,
    height: 18
  },
  title: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 57,
    width: restrictW - 36,
    height: 18
  },
  titleText: {
    position: "absolute",
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff"
  },
  share: {
    marginTop: 57,
    width: 18,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "rgb(251,251,252)",
    width,
    height
  },
  posterImages: {
    display: "flex",
    alignItems: "center",
    width,
    height: width
  },
  slide: {
    flex: 1,
    backgroundColor: "transparent"
  },
  tagsWrapper: {
    marginTop: 5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignSelf: "center",
    flexWrap: "wrap",
    width: restrictW
  },
  tagContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 10,
    marginBottom: 10,
    height: 26,
    borderRadius: 13,
    backgroundColor: "rgba(216,216,216,0.3)"
  },
  tagLabel: {
    color: "rgb(253,92,99)"
  },
  profile: {
    position: "relative",
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    width: restrictW,
    height: 60
  },
  infoDetail: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "flex-start",
    width: restrictW - 70,
    marginLeft: 10,
    height: 60
  },
  infoHead: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  followBtn: {
    width: 57,
    height: 22,
    borderRadius: 4,
    backgroundColor: "rgb(253,92,99)"
  },
  value: {
    height: 24,
    fontSize: 20,
    textAlign: "center",
    color: "black",
    fontWeight: "bold"
  },
  desc: {
    height: 15,
    fontSize: 12,
    textAlign: "center",
    color: "rgb(155,155,155)"
  },
  mask: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    bottom: 0,
    width: 230,
    height: 40,
    opacity: 0.8
  },
  maskText: {
    textAlign: "center",
    color: "#fff",
    fontSize: 14
  },
  dataInfo: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 16,
    width: restrictW,
    height: 60
  },
  itemBox: {
    width: restrictW / 4,
    height: 40
  },
  wrapper: {
    width,
    // height,
    // height: 300,
    backgroundColor: "rgb(251,251,252)",
    marginBottom: 60
  },
  tabBars: {
    display: "flex",
    alignSelf: "center",
    alignItems: "center",
    flexDirection: "row",
    width: restrictW / 2,
    height: 50
  },
  tabBarsText: {
    fontSize: 16,
    color: "rgb(51,51,51)"
  },
  tabBarsTextAc: {
    fontSize: 16,
    color: "rgb(211,211,211)"
  },
  flatView: {
    marginLeft: 20,
    paddingTop: 20,
    display: "flex",
    justifyContent: "center",
    width: width - 40,
    marginBottom: 50
  },
  postTitle: {
    display: "flex",
    justifyContent: "flex-start",
    marginTop: 6,
    marginBottom: 6,
    width: width - 40
  },
  controlBar: {
    position: "absolute",
    bottom: 0,
    display: "flex",
    flexDirection: "row",
    width,
    height: 85.2,
    borderTopWidth: 0.5,
    borderTopColor: "rgba(0,0,0,0.1)",
    backgroundColor: "#fff"
  },
  profileBox: {
    marginLeft: 20,
    marginTop: 12.5,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignSelf: "flex-start",
    width: 100,
    height: 30
  },
  profileDetail: {
    width: 150,
    height: 15,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft: 10
  },
  profileDetailName: {
    fontSize: 11,
    color: "rgb(51,51,51)",
    marginRight: 10
  },
  profileDetailFollow: {
    fontSize: 11,
    color: "rgb(253,92,99)",
    fontWeight: "bold"
  },
  postTime: {
    marginLeft: 10,
    fontSize: 10,
    color: "rgb(155,155,155)"
  },
  thumbs: {
    position: "absolute",
    right: 30,
    top: 12.5,
    width: 50,
    height: 30,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf: "flex-start"
  },
  thumbesBox: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    alignSelf: "flex-start"
  },
  thumbsImage: {
    width: 20,
    height: 18
  },
  thumbsText: {
    marginLeft: 6,
    fontSize: 16,
    color: "rgb(253,92,99)"
  }
});
