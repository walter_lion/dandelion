import React, { Component } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import { Avatar, ButtonGroup } from "react-native-elements";
import { fetchNotiFollow, fetchNotiLike } from "../actions/notifications";

import { followingUser, unFollowUser } from "../actions/user";
import moment from "moment";
const { width, height } = Dimensions.get("window");
const fontSize = 16;
const close = require("../assets/icons/cancel.png");
import { assetRoot } from "../constants/urlConstant";

const Header = ({ styles, opacity, leftEvent, rightEvent, title }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

class LikeItem extends Component {
  constructor(props) {
    super(props);
  }

  goToPoster = () => {
    const { navigation, postId } = this.props;
    navigation.navigate("Poster", {
      postId,
      likePost: 0,
      unLikePost: 0
    });
  };
  render() {
    const {
      id,
      userName,
      time,
      postTitle,
      followHandle,
      state,
      avatarPhoto
    } = this.props;
    return (
      <View style={styles.item}>
        <Avatar
          medium
          rounded
          source={{
            uri: `${assetRoot}/${avatarPhoto}`
          }}
          onPress={() => console.log("Works!")}
          activeOpacity={0.7}
        />
        <View
          style={{
            width: width - 80,
            height: "100%",
            marginLeft: 10,
            display: "flex"
          }}
        >
          <TouchableOpacity onPress={this.goToPoster}>
            <View style={{ width: width - 80, height: 33 }}>
              <Text>
                <Text style={{ fontSize: 14, color: "rgb(51,51,51)" }}>
                  {userName}
                </Text>
                <Text style={{ fontSize: 12, color: "rgb(155,155,155)" }}>
                  {" "}
                  {`liked your post <`}
                </Text>
                <Text
                  style={{
                    fontSize: 12,
                    color: "rgb(253,92,99)",
                    textDecorationLine: "underline"
                  }}
                >
                  {postTitle}
                </Text>
                <Text style={{ fontSize: 12, color: "rgb(155,155,155)" }}>
                  {" "}
                  >{" "}
                </Text>
              </Text>
            </View>
          </TouchableOpacity>

          <Text style={{ fontSize: 10, color: "rgb(155,155,155)" }}>
            {time}
          </Text>
        </View>
      </View>
    );
  }
}

class FollowItem extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentDidMount() {}

  itemHandle = () => {
    const { id, state, followHandle, unFollowHandle } = this.props;
    if (state === 0) {
      followHandle(id);
    } else if (state === 1) {
      unFollowHandle(id);
    }
  };

  render() {
    const { id, userName, time, followHandle, state, avatarPhoto } = this.props;
    return (
      <View style={styles.item}>
        <Avatar
          medium
          rounded
          source={{
            uri: `${assetRoot}/${avatarPhoto}`
          }}
          onPress={() => console.log("Works!")}
          activeOpacity={0.7}
        />
        <View
          style={{
            width: width - 80,
            height: "100%",
            marginLeft: 10,
            display: "flex",
            flexDirection: "row",
            alignSelf: "center"
          }}
        >
          <View
            style={{
              width: width - 150,
              height: 33,
              display: "flex",
              flexDirection: "column"
            }}
          >
            <Text style={{ fontSize: 14, color: "rgb(51,51,51)" }}>
              {userName}
            </Text>
            <Text style={{ fontSize: 10, color: "rgb(155,155,155)" }}>
              {time}
            </Text>
          </View>
          <TouchableOpacity onPress={this.itemHandle}>
            <View style={styles.followButt}>
              <Text style={{ fontSize: 10, color: "#fff" }}>
                {state === 1 ? "+ Follow" : "- Unfollow"}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default class Notifications extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
      selectedIndex: 0,
      pages: {},
      listData: {
        FOLLOW: {
          pagination: {},
          content: []
        },
        LIKES: {
          pagination: {},
          content: []
        }
      }
    };
  }

  updateIndex = selectedIndex => {
    this.setState({ selectedIndex });
  };

  handleSubmit = () => {
    const { navigation } = this.props;
    navigation.navigate("Home");
  };
  goToSignUp = () => {
    const { navigation } = this.props;
    navigation.navigate("SignUp");
  };
  componentDidMount() {
    this.fetchInitData();
  }

  fetchInitData = () => {
    this.setState({
      isRefreshing: true
    });
    Promise.all([
      this.fetchNotiFollowHandle(),
      this.fetchNotiLikeHandle()
    ]).then(res => {
      this.setState({
        isRefreshing: false
      });
    });
  };

  onLoaderMore = () => {
    const { selectedIndex, listData } = this.state;
    if (selectedIndex === 0) {
      const pagination = listData.FOLLOW.pagination;
      if (!pagination.last) {
        this.setState({
          isLoading: true
        });
        this.fetchNotiFollowHandle(pagination.currentPage + 1).then(res => {
          this.setState({
            isLoading: false
          });
        });
      }
    } else {
      const pagination = listData.LIKES.pagination;
      if (!pagination.last) {
        this.setState({
          isLoading: true
        });
        this.fetchNotiLikeHandle(pagination.currentPage + 1).then(res => {
          this.setState({
            isLoading: false
          });
        });
      }
    }
  };
  fetchNotiFollowHandle = (page = 0) => {
    let { listData } = this.state;
    return fetchNotiFollow(page, 10).then(res => {
      const { code, data } = res;
      if (code === 200) {
        listData["FOLLOW"].pagination.currentPage = data.number;
        listData["FOLLOW"].pagination.totalPages = data.totalPages;
        listData["FOLLOW"].pagination.last = data.last;
        if (page === 0) {
          listData["FOLLOW"].content = data.content;
        } else {
          listData["FOLLOW"].content = (
            (listData["FOLLOW"] && listData["FOLLOW"].content) ||
            []
          ).concat(data.content);
        }
        this.setState({
          listData
        });
        return Promise.resolve("success");
      } else {
        return Promise.reject("failure");
      }
    });
  };
  fetchNotiLikeHandle = (page = 0) => {
    let { listData } = this.state;
    return fetchNotiLike(page, 10).then(res => {
      const { code, data } = res;
      if (code === 200) {
        listData["LIKES"].pagination.currentPage = data.number;
        listData["LIKES"].pagination.totalPages = data.totalPages;
        listData["LIKES"].pagination.last = data.last;
        if (page === 0) {
          listData["LIKES"].content = data.content;
        } else {
          listData["LIKES"].content = (
            (listData["LIKES"] && listData["LIKES"].content) ||
            []
          ).concat(data.content);
        }
        this.setState({
          listData
        });
        return Promise.resolve("success");
      }
    });
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  followUserHandle = id => {
    let { listData } = this.state;
    followingUser(id).then(res => {
      const { code } = res;
      if (code === 200) {
        listData["FOLLOW"].content = listData["FOLLOW"].content.map(
          user => (user.userId === id ? { ...user, state: 1 } : user)
        );
        this.setState({
          listData
        });
      }
    });
  };

  unFollowUserHandle = id => {
    let { listData } = this.state;
    unFollowUser(id).then(res => {
      const { code } = res;
      if (code === 200) {
        listData["FOLLOW"].content = listData["FOLLOW"].content.map(
          user => (user.userId === id ? { ...user, state: 0 } : user)
        );

        this.setState({
          listData
        });
      }
    });
  };

  renderFooter = () => {
    const { selectedIndex, listData, isLoading } = this.state;
    let last = false;
    if (selectedIndex === 0) {
      last = listData.FOLLOW.pagination.last;
    } else {
      last = listData.LIKES.pagination.last;
    }
    if (!isLoading && last) {
      return (
        <View
          style={{
            height: 30,
            alignItems: "center",
            justifyContent: "flex-start"
          }}
        >
          <Text
            style={{
              color: "#999999",
              fontSize: 14,
              marginTop: 5,
              marginBottom: 5
            }}
          >
            No more
          </Text>
        </View>
      );
    } else if (isLoading) {
      return (
        <View>
          <ActivityIndicator />
        </View>
      );
    } else if (!isLoading && !last) {
      return (
        <View>
          <Text />
        </View>
      );
    }
  };

  render() {
    const buttons = ["New Followers", "New Likes"];
    const { isRefreshing, selectedIndex, listData } = this.state;
    const { navigation } = this.props;
    const { FOLLOW, LIKES } = listData;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Notifications"}
          leftEvent={this.goBack}
        />
        <View style={styles.topBar}>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={buttons}
            buttonStyle={{
              width: (width - 20) / 2,
              height: 40,
              padding: 0,
              backgroundColor: "#fff"
            }}
            selectedButtonStyle={{
              backgroundColor: "rgb(253,92,99)",
              width: (width - 20) / 2,
              height: 40,
              padding: 0,
              margin: 0
            }}
            textStyle={{ color: "rgb(253,92,99)" }}
            selectedTextStyle={{ color: "#fff" }}
            containerStyle={{
              height: 40,
              width: width - 20,
              borderColor: "rgb(253,92,99)"
            }}
          />
        </View>
        <View style={styles.wrapper}>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            refreshing={isRefreshing}
            onRefresh={this.fetchInitData}
            onEndReached={this.onLoaderMore}
            ListFooterComponent={this.renderFooter}
            data={
              selectedIndex === 0 ? FOLLOW.content || [] : LIKES.content || []
            }
            renderItem={({ item }) =>
              selectedIndex === 0 ? (
                <FollowItem
                  id={item.userId}
                  time={moment(item.followingTime).fromNow()}
                  state={item.state}
                  userName={item.userName}
                  avatarPhoto={item.avatarPhoto}
                  followHandle={this.followUserHandle}
                  unFollowHandle={this.unFollowUserHandle}
                />
              ) : (
                <LikeItem
                  id={item.userId}
                  time={moment(item.likeTime).fromNow()}
                  postTitle={item.postTitle}
                  userName={item.userName}
                  avatarPhoto={item.avatarPhoto}
                  navigation={navigation}
                  postId={item.postId}
                />
              )
            }
          />
        </View>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 48,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    backgroundColor: "rgb(251,251,252)"
  },
  topBar: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width,
    height: 60,
    padding: 5
  },
  button: {
    marginTop: 20,
    width: 263,
    height: 40,
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 4
  },
  wrapper: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width,
    height: height - 148,
    backgroundColor: "rgb(251,251,252)"
  },
  item: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    paddingLeft: 10,
    paddingRight: 10,
    width,
    height: 50,
    marginTop: 10
  },
  itemText: {
    width: width - 50,
    height: "100%",
    marginLeft: 10,
    display: "flex",
    flexDirection: "row"
  },
  followButt: {
    marginTop: 10,
    right: 6,
    height: 22,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 6,
    paddingRight: 6,
    borderRadius: 4,
    backgroundColor: "rgb(253,92,99)"
  }
});
