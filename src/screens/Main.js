import BottomTab from '../components/bottomTab/BottomTab'
import EditProfile from './EditProfile'
import Home from './home/Home'
import Myprofile from './Myprofile'
import Post from './post/Post'
import { createBottomTabNavigator , createStackNavigator} from 'react-navigation';

const Tabs = createBottomTabNavigator({
  Home: Home,
  Myprofile: Myprofile,
},{
  tabBarOptions: {
    style: {
      borderTopWidth: 0,
      backgroundColor: 'transparent'
    }
  },
  initialRouteName: 'Home',
  tabBarComponent: BottomTab
});



const RootStack = createStackNavigator({
  Tabs: {
    screen: Tabs,
  },
  Post: {
    screen: Post,
    navigationOptions: {
      gesturesEnabled: false,
    },
  },
  EditProfile: {
    screen: EditProfile
  }
}, {
  headerMode: 'none',
  mode: 'modal',
});



export default RootStack
