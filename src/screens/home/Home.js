import Header from "../../components/header/Header";
import PostItem from "../../components/PostItem/PostItem";
import React, { Component } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableWithoutFeedback,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl,
  Alert
} from "react-native";
import ScrollableTabView, {
  DefaultTabBar,
  ScrollableTabBar
} from "react-native-scrollable-tab-view";
import Swiper from "react-native-swiper";
import Overlay from "../../components/custom/Overlay";

import * as serviceTool from "../../utils/serviceTool";
import { assetRoot, POST } from "../../constants/urlConstant";
import { fetchPost, likePostById, unLikePostById } from "../../actions/post";
const { width, height } = Dimensions.get("window");
const postItemWidth = (width - 27) / 2;
const postItemHeight = (postItemWidth * 200) / 174;
const middleIcon = require("../../assets/icons/dandelion_logo.png");

const PostFlatList = ({
  postList,
  isRefreshing,
  tabLabel,
  navigation,
  likePost,
  unLikePost,
  postItemWidth,
  postItemHeight,
  loading,
  isLast
}) => {
  return (
    <View
      tabLabel={tabLabel}
      style={{
        ...styles.flatList,
        marginTop: 10,
        marginBottom: 80
      }}
    >
      <FlatList
        keyExtractor={(item, index) => index.toString()}
        tabLabel={tabLabel}
        scrollEnabled={false}
        refreshing={isRefreshing}
        scrollEventThrottle={1}
        getItemLayout={(data, index) => ({
          length: 100,
          offset: (100 + 2) * index,
          index
        })}
        data={postList}
        columnWrapperStyle={{ padding: 10 }}
        numColumns={2}
        renderItem={({ item }) => (
          <PostItem
            key={item.postId}
            navigation={navigation}
            id={item.postId}
            index={item.postId}
            width={postItemWidth}
            height={postItemHeight}
            postData={item}
            likePost={likePost}
            unLikePost={unLikePost}
          />
        )}
      />
      {!isLast && loading && <ActivityIndicator />}
      {isLast && (
        <View style={styles.noMoreTip}>
          <Text style={styles.noMoreTipText}>No more</Text>
        </View>
      )}
    </View>
  );
};

export default class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPosting: false,
      isFetched: false,
      isFetching: false,
      isRefreshing: false,
      scrollEnabe: true,
      scrollHeight: 0,
      banners: [],
      typeList: [],
      postList: {}
    };
  }

  switchType = ({ i: index }) => {
    let { typeList, postList } = this.state;
    const type = typeList[index].name;
    if (!(type in postList)) {
      fetchPost(0, type, 0, 10).then(res => {
        const { code, data } = res;
        if (code === 200) {
          let currentHotData = data.hot;
          let currentOtherData = data.other;
          let currentData = {
            content: []
          };
          if (currentHotData !== null && currentHotData.content) {
            currentData.pagination = {
              currentPage: currentHotData.number,
              totalPages: currentHotData.totalPages,
              last: currentHotData.last
            };
            currentData.content = currentData.content.concat(
              currentHotData.content
            );
          }

          if (currentOtherData !== null && currentOtherData.content) {
            currentData.pagination = {
              currentPage: currentOtherData.number,
              totalPages: currentOtherData.totalPages,
              last: currentOtherData.last
            };

            currentData.content = currentData.content.concat(
              currentOtherData.content
            );
          }

          postList[type] = currentData;
          this.setState({
            isFetching: false,
            isFetched: true,
            typeList: typeList.map(
              v =>
                v.name === type
                  ? { ...v, selected: true }
                  : { ...v, selected: false }
            ),
            postList
          });
        }
      });
    }
  };

  componentDidMount() {
    this.setState({
      isFetching: true
    });
    fetchPost(1, "hot", 0, 10)
      .then(res => {
        const { code, data } = res;
        if (code === 200) {
          let currentHotData = data.hot;
          let currentOtherData = data.other;
          let currentData = {
            content: []
          };
          if (currentHotData !== null && currentHotData.content) {
            currentData.pagination = {
              currentPage: currentHotData.number,
              totalPages: currentHotData.totalPages,
              last: currentHotData.last
            };
            currentData.content = currentData.content.concat(
              currentHotData.content
            );
          }

          if (currentOtherData !== null && currentOtherData.content) {
            currentData.pagination = {
              currentPage: currentOtherData.number,
              totalPages: currentOtherData.totalPages,
              last: currentOtherData.last
            };
            currentData.content = currentData.content.concat(
              currentOtherData.content
            );
          }

          this.setState({
            isFetching: false,
            isFetched: true,
            banners: data.banners,
            typeList: data.types.map(v => ({
              ...v,
              selected: v.name === "hot" ? true : false
            })),
            postList: {
              hot: currentData
            }
          });
        } else {
          this.setState({
            isFetching: false,
            isFetched: true
          });
        }
      })
      .catch(err => {
        this.setState({
          isFetching: false,
          isFetched: true
        });
      });
  }
  _onEndReached = () => {
    const { typeList, postList } = this.state;
    const currentType = typeList.find(item => item.selected).name;
    let currentPostData = postList[currentType];
    let pagination = currentPostData.pagination;
    this.setState({
      isFetching: true
    });
    if (!pagination.last) {
      fetchPost(0, currentType, pagination.currentPage + 1, 10)
        .then(res => {
          const { code, data } = res;
          if (code === 200) {
            if (data.hot !== null && data.hot.content) {
              pagination.currentPage = data.hot.number;
              pagination.last = data.hot.last;
              currentPostData.content = currentPostData.content.concat(
                data.hot.content
              );
            }

            if (data.other !== null && data.other.content) {
              pagination.currentPage = data.other.number;
              pagination.last = data.other.last;
              currentPostData.content = currentPostData.content.concat(
                data.other.content
              );
            }
            this.setState({
              postList,
              isFetching: false
            });
          } else {
            this.setState({
              isFetching: false
            });
          }
        })
        .catch(err => {
          this.setState({
            isFetching: false
          });
        });
    } else {
      this.setState({
        isFetching: false
      });
    }
  };
  handleRefresh() {
    let { typeList, postList } = this.state;
    const currentType = typeList.find(item => item.selected).name;
    let currentPostData = postList[currentType];
    this.setState({
      isRefreshing: true
    });
    console.log("isRefreshing");
    fetchPost(0, currentType, 0, 10).then(res => {
      const { code, data } = res;
      if (code === 200) {
        let currentHotData = data.hot;
        let currentOtherData = data.other;
        let currentData = {
          content: []
        };
        if (currentHotData !== null && currentHotData.content) {
          currentData.pagination = {
            currentPage: currentHotData.number,
            totalPages: currentHotData.totalPages,
            last: currentHotData.last
          };
          currentData.content = currentData.content.concat(
            currentHotData.content
          );
        }

        if (currentOtherData !== null && currentOtherData.content) {
          currentData.pagination = {
            currentPage: currentOtherData.number,
            totalPages: currentOtherData.totalPages,
            last: currentOtherData.last
          };
          currentData.content = currentData.content.concat(
            currentOtherData.content
          );
        }
        postList[currentType] = currentData;
        currentPostData = currentData;
        this.setState({
          postList,
          isRefreshing: false
        });
      }
    });
  }

  _likePost = postId => {
    let { typeList, postList, isPosting } = this.state;
    if (!isPosting) {
      const currentType = typeList.find(item => item.selected).name;
      let currentPostData = postList[currentType];
      currentPostData.content = currentPostData.content
        .filter(item => item !== null)
        .map(
          item =>
            item.postId === postId
              ? { ...item, isLiked: 1, postLikes: item.postLikes + 1 }
              : item
        );
      postList[currentType] = currentPostData;
      this.setState({
        postList,
        isPosting: true
      });
      likePostById(postId).then(res => {
        const { code } = res;
        if (code === 200) {
          this.setState({
            isPosting: false
          });
        }
      });
    }
  };
  _unLikePost = postId => {
    let { typeList, postList, isPosting } = this.state;
    if (!isPosting) {
      const currentType = typeList.find(item => item.selected).name;
      let currentPostData = postList[currentType];
      currentPostData.content = currentPostData.content
        .filter(item => item !== null)
        .map(
          item =>
            item.postId === postId
              ? { ...item, isLiked: -1, postLikes: item.postLikes - 1 }
              : item
        );
      postList[currentType] = currentPostData;
      this.setState({
        postList,
        isPosting: true
      });
      unLikePostById(postId).then(res => {
        const { code } = res;
        if (code === 200) {
          this.setState({
            isPosting: false
          });
        }
      });
    }
  };

  onScrollHandle = event => {
    const { isFetching, isRefreshing, typeList, postList } = this.state;
    const selectedList = typeList.find(item => item.selected);
    if (selectedList) {
      const currentType = selectedList.name;
      const last = postList[currentType].pagination.last;
      if (isFetching || isRefreshing || last) {
        return;
      }
      let y = event.nativeEvent.contentOffset.y;
      let height = event.nativeEvent.layoutMeasurement.height;
      let contentHeight = event.nativeEvent.contentSize.height;
      if (y + height >= contentHeight - 20) {
        this._onEndReached();
      }

      if (y < 20 && !isRefreshing) {
        this.handleRefresh();
      }
    }
  };

  goToPostByBanner = bannerId => {
    const { navigation } = this.props;
    navigation.navigate("Poster", {
      postId: bannerId,
      likePost: this._likePost,
      unLikePost: this._unLikePost
    });
  };

  render() {
    const {
      isFetching,
      isFetched,
      isRefreshing,
      scrollEnabe,
      scrollHeight,
      banners,
      typeList,
      postList
    } = this.state;
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        <Header middleIcon={middleIcon} />
        <View style={styles.wrapper}>
          <ScrollableTabView
            onChangeTab={this.switchType}
            onContainerScroll={this.onScrollHandle}
            collapsableBar={
              <View
                style={{
                  width,
                  height: 144 - scrollHeight
                }}
              >
                <Swiper
                  index={0}
                  key={banners.length}
                  width={width}
                  height={144 - scrollHeight}
                  autoplay={true}
                  autoplayTimeout={3}
                  showsPagination={true}
                  dot={
                    <View
                      style={{
                        backgroundColor: "rgba(255,255,255,.3)",
                        width: 4,
                        height: 4,
                        borderRadius: 2,
                        marginLeft: 7,
                        marginRight: 7
                      }}
                    />
                  }
                  activeDot={
                    <View
                      style={{
                        backgroundColor: "transparent",
                        width: 13,
                        height: 13,
                        borderRadius: 7.5,
                        borderWidth: 0.5,
                        borderColor: "#fff"
                      }}
                    >
                      <View
                        style={{
                          backgroundColor: "#fff",
                          width: 4,
                          height: 4,
                          borderRadius: 2,
                          marginLeft: 4,
                          marginTop: 4
                        }}
                      />
                    </View>
                  }
                  paginationStyle={{
                    bottom: 10
                  }}
                  loop={true}
                >
                  {banners.map((banner, index) => (
                    <View
                      style={{
                        ...styles.slide,
                        height: 144 - scrollHeight
                      }}
                      key={"banner" + index}
                    >
                      <TouchableWithoutFeedback
                        onPress={this.goToPostByBanner.bind(
                          this,
                          banner.bannerId
                        )}
                      >
                        <Image
                          style={{
                            ...styles.image,
                            height: 144 - scrollHeight
                          }}
                          source={
                            { uri: `${assetRoot}/${banner.photoUrl}` } ||
                            require("../../assets/icons/banner.jpg")
                          }
                          resizeMode="cover"
                        />
                      </TouchableWithoutFeedback>
                    </View>
                  ))}
                </Swiper>
              </View>
            }
            tabBarBackgroundColor="white"
            initialPage={0}
            renderTabBar={() => (
              <ScrollableTabBar
                activeTextColor="rgb(253,92,99)"
                tabBarTextStyle={{
                  fontWeight: 100
                }}
                underlineStyle={{
                  position: "absolute",
                  height: 4,
                  backgroundColor: "rgb(253,92,99)",
                  bottom: 0
                }}
              />
            )}
          >
            {typeList.map((type, index) => (
              <PostFlatList
                key={index}
                tabLabel={type.name.toUpperCase()}
                postList={
                  (postList[type.name] && postList[type.name].content) || []
                }
                isLast={
                  postList[type.name] && postList[type.name].pagination.last
                }
                isRefreshing={isRefreshing}
                navigation={navigation}
                likePost={this._likePost}
                unLikePost={this._unLikePost}
                postItemWidth={postItemWidth}
                postItemHeight={postItemHeight}
                loading={isFetching}
              />
            ))}
          </ScrollableTabView>
        </View>
        {!isFetched && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  wrapper: {
    width,
    height: height - 88,
    backgroundColor: "rgb(251,251,252)"
  },
  slide: {
    width,
    height: 144,
    backgroundColor: "transparent"
  },
  image: {
    width,
    height: 144
  },
  flatList: {
    width
  },
  noMoreTip: {
    width,
    height: 40,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 10
  },
  noMoreTipText: {
    color: "rgb(155,155,155)",
    fontSize: 14
  },
  catalogWrapper: {
    display: "flex",
    alignSelf: "center",
    flexDirection: "row",
    width: width - 40,
    height: 35
  },
  catalog: {
    marginLeft: 10,
    marginRight: 10,
    display: "flex",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center"
  },
  catalogText: {
    color: "rgb(51,51,51)",
    fontSize: 12
  }
});
