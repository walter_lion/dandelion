const close = require("../../assets/icons/cancel.png");
const camera = require("../../assets/icons/camera.png");
// import Header from '../../components/header/Header'
import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View,
  ActivityIndicator,
  Alert
} from "react-native";
import ImagePicker from "react-native-image-crop-picker";
import Overlay from "../../components/custom/Overlay";
import { fetchPostDetail } from "../../actions/post";
import { assetRoot } from "../../constants/urlConstant";
const { width, height } = Dimensions.get("window");
const restrictW = width - 40;

const imageWidth = (width - 35) / 4;
var fetchParams = {
  first: 20,
  groupTypes: "All",
  groupName: "Camera Roll",
  assetType: "Photos"
};

var fetchAllParams = {
  first: 20,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({ styles, opacity, leftEvent, rightEvent, avaliable }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      {avaliable && (
        <TouchableOpacity onPress={rightEvent}>
          <View style={styles.share}>
            <Text
              style={{
                color: "rgb(253,92,99)",
                fontSize: 14,
                fontWeight: "bold"
              }}
            >
              NEXT
            </Text>
          </View>
        </TouchableOpacity>
      )}
    </View>
  );
};

const ImageItem = ({ url, selected, index, selectImage }) => {
  return (
    <TouchableOpacity onPress={selectImage}>
      <View style={styles.imageItem}>
        {index > -1 && (
          <View style={styles.imageSelected}>
            <View style={styles.dot}>
              <Text
                style={{
                  width: 15,
                  height: 15,
                  textAlign: "center",
                  color: "rgb(253,92,99)"
                }}
              >
                {index + 1}
              </Text>
            </View>
          </View>
        )}
        <Image
          style={{ width: imageWidth, height: imageWidth, borderRadius: 4 }}
          source={{ uri: url }}
        />
      </View>
    </TouchableOpacity>
  );
};

export default class SelectImage extends Component {
  constructor() {
    super();
    this.state = {
      isFetching: false,
      postDetail: null,
      selectList: [],
      imageList: [],
      showImage: ""
    };
  }

  componentDidMount() {
    const { navigation } = this.props;

    const {
      state: { params }
    } = navigation;
    this.setState({
      isFetching: true
    });

    if (params && params.postId) {
      this.fetchData(params.postId).then(res => {
        var promise = CameraRoll.getPhotos(fetchParams);
        promise
          .then(data => {
            var edges = data.edges;
            var photos = [];
            for (var i in edges) {
              photos.push({
                url: edges[i].node.image.uri,
                isSurface: 0
              });
            }
            this.setState({
              imageList: res.photos.concat(photos),
              selectList: res.photos.map(item => item.url),
              postDetail: {
                postId: res.post.postId,
                tags: res.tags,
                title: res.post.title,
                content: res.post.content,
                category: res.post.category
              },
              showImage: res.photos[0].url,
              isFetching: false
            });
          })
          .catch(() => {
            var allPhotoPromise = CameraRoll.getPhotos(fetchAllParams);
            allPhotoPromise
              .then(data => {
                var edges = data.edges;
                var photos = [];
                for (var i in edges) {
                  photos.push({
                    url: edges[i].node.image.uri,
                    isSurface: 0
                  });
                }
                this.setState({
                  imageList: res.photos.concat(photos),
                  selectList: res.photos.map(item => item.url),
                  postDetail: {
                    postId: res.post.postId,
                    tags: res.tags,
                    title: res.post.title,
                    content: res.post.content,
                    category: res.post.category
                  },
                  showImage: res.photos[0].url,
                  isFetching: false
                });
              })
              .catch(() => {
                this.setState({
                  isFetching: false
                });
              });
          });
      });
    } else {
      //获取照片
      var promise = CameraRoll.getPhotos(fetchParams);
      promise
        .then(data => {
          var edges = data.edges;
          var photos = [];
          for (var i in edges) {
            photos.push({
              url: edges[i].node.image.uri,
              isSurface: 0
            });
          }
          this.setState({
            imageList: photos,
            showImage: photos[0].url,
            isFetching: false
          });
        })
        .catch(() => {
          var allPhotoPromise = CameraRoll.getPhotos(fetchAllParams);
          allPhotoPromise
            .then(data => {
              var edges = data.edges;
              var photos = [];
              for (var i in edges) {
                photos.push({
                  url: edges[i].node.image.uri,
                  isSurface: 0
                });
              }
              this.setState({
                imageList: photos,
                showImage: photos[0].url,
                isFetching: false
              });
            })
            .catch(() => {
              this.setState({
                isFetching: false
              });
            });
        });
    }
  }

  fetchData = postId => {
    return fetchPostDetail(postId).then(res => {
      const { code, data } = res;
      if (code === 200) {
        return Promise.resolve(data);
      } else {
        return Promise.reject(code);
      }
    });
  };

  wakeUpCamera = e => {
    ImagePicker.openPicker({
      width: 755,
      height: 755,
      cropping: true
    }).then(image => {
      const currentImage = { url: image.path, isSurface: 0 };
      this.setState({
        imageList: [currentImage].concat(this.state.imageList)
      });
    });
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.navigate("Tabs");
  };

  goNext = () => {
    const { navigation } = this.props;
    const { selectList, imageList, postDetail } = this.state;
    const selectImages = selectList.map(url =>
      imageList.find(item => item.url === url)
    );

    let sendMessage = {
      imageList: selectImages.map((item, index) => ({
        ...item,
        isSurface: index === 0 ? 1 : 0
      }))
    };
    if (postDetail !== null) {
      sendMessage.postDetail = postDetail;
    }
    navigation.navigate("EditPost", sendMessage);
  };

  selectImage = url => {
    let { postList, selectList } = this.state;
    const index = selectList.indexOf(url);

    if (index === -1 && selectList.length < 9) {
      selectList = selectList.concat(url);
      this.setState({ selectList, showImage: url });
    } else {
      if (index !== -1) {
        selectList.splice(index, 1);
        this.setState({ selectList, showImage: url });
      }
    }
  };

  render() {
    const { selectList, imageList, isFetching, showImage } = this.state;
    const cameraItem = [{ id: "mock" }];
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          leftEvent={this.goBack}
          rightEvent={this.goNext}
          avaliable={selectList.length > 0}
        />
        <View style={styles.mainContainer}>
          {
            <Image
              style={{
                width: "100%",
                height: "100%",
                borderRadius: 4,
                backgroundColor: "rgba(0,0,0,0.2)"
              }}
              source={{
                uri:
                  imageList.length && imageList[0].postPhotoId
                    ? `${assetRoot}/${showImage}`
                    : showImage
              }}
            />
          }
        </View>
        <View style={styles.selectContainer}>
          <FlatList
            keyExtractor={(item, index) => index.toString()}
            data={cameraItem.concat(imageList)}
            numColumns={4}
            renderItem={({ item, index }) =>
              index === 0 ? (
                <TouchableOpacity onPress={this.wakeUpCamera}>
                  <View style={styles.openCamera}>
                    <Image style={{ width: 20, height: 17 }} source={camera} />
                  </View>
                </TouchableOpacity>
              ) : (
                <ImageItem
                  url={item.postPhotoId ? `${assetRoot}/${item.url}` : item.url}
                  selected={item.isSurface === 1}
                  selectImage={this.selectImage.bind(null, item.url)}
                  index={selectList.indexOf(item.url)}
                />
              )
            }
          />
        </View>
        {isFetching && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 40,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    width: restrictW - 36,
    height: 18
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  openCamera: {
    position: "relative",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    margin: 2.5,
    borderRadius: 4,
    width: imageWidth,
    height: imageWidth,
    backgroundColor: "rgb(253,92,99)"
  },
  mainContainer: {
    margin: 10,
    width: width - 20,
    height: width - 20
  },
  selectContainer: {
    width: width - 16,
    flex: 2,
    justifyContent: "flex-start"
    // alignItems: "center"
  },
  imageItem: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: imageWidth,
    height: imageWidth
  },
  imageSelected: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    left: 0,
    borderRadius: 4,
    width: imageWidth,
    height: imageWidth,
    backgroundColor: "rgba(253,92,99,0.5)",
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    width,
    height: height - 88,
    backgroundColor: "rgb(251,251,252)"
  }
});
