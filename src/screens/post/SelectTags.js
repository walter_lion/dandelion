const deleteIcon = require("../../assets/icons/delete.png");
const tagIcon = require("../../assets/icons/tag.png");

import React, { Component } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { BlurView, VibrancyView } from "react-native-blur";
import { SearchBar } from "react-native-elements";
import { getDefaultTag, searchTags, saveTag } from "../../actions/post";
const { width, height } = Dimensions.get("window");

const TagItem = ({ id, name }) => {
  return (
    <View style={styles.tagContainer}>
      <Image source={tagIcon} />
      <Text style={{ color: "#fff", fontSize: 12, marginLeft: 6 }}>{name}</Text>
    </View>
  );
};
export default class SelectTags extends Component {
  constructor() {
    super();
    this.state = {
      searchName: "",
      defaulTags: {
        history: [],
        hot: []
      },
      searchTag: [],
      choosedTags: []
    };
  }

  componentDidMount() {
    getDefaultTag(null).then(res => {
      const {
        code,
        data: { history, hot }
      } = res;
      if (code === 200) {
        this.setState({
          defaulTags: {
            history: history || [],
            hot: hot || []
          }
        });
      }
    });
  }

  searchTagByName = searchName => {
    const { category } = this.props;
    this.setState(
      {
        searchName
      },
      () => {
        let options = "";
        if (searchName.trim().length) {
          options = "?tagName=" + searchName.trim();

          if (category !== null) {
            options = options + "&catalog=" + category;
          }
        }

        searchTags(options).then(res => {
          const { code, data } = res;
          if (code === 200) {
            this.setState({
              searchTag: data
            });
          }
        });
      }
    );
  };

  clearSearchTagName = () => {
    this.refs.searchBar.clear;
    this.setState({
      searchName: ""
    });
  };

  chooseTag = tag => {
    this.props.chooseTagHandle(tag);
  };

  createTag = tagName => {
    saveTag(tagName).then(res => {
      const { code, data } = res;
      if (code === 200) {
        this.setState({
          defaulTags: {
            ...this.state.defaulTags,
            history: this.state.defaulTags.history.concat(data)
          }
        });
        this.props.chooseTagHandle(data);
      }
    });
  };

  clearHistoryTag = () => {
    this.setState({
      defaulTags: {
        ...this.state.defaulTags,
        history: []
      }
    });
  };

  renderSearchTags = tags => {
    const createTag =
      this.state.searchName.length > 0
        ? { creat: true, tagName: this.state.searchName }
        : { creat: false, tagName: this.state.searchName };
    return (
      <View style={styles.searchResultContainer}>
        {createTag.creat && (
          <View style={{ marginBottom: 20 }}>
            <View>
              <Text style={{ fontSize: 14, color: "#fff" }}>Create Tag</Text>
            </View>
            <TouchableOpacity
              onPress={this.createTag.bind(null, createTag.tagName)}
            >
              <View style={styles.tagPickItem}>
                <View style={styles.tagPickItemInner}>
                  <Image source={tagIcon} style={{ marginTop: 0 }} />
                  <View style={styles.searchTagDetail}>
                    <Text style={{ color: "rgb(255,255,255)", fontSize: 14 }}>
                      {createTag.tagName}
                    </Text>
                  </View>
                </View>
              </View>
            </TouchableOpacity>
          </View>
        )}
        <View>
          <View>
            <Text style={{ fontSize: 14, color: "#fff" }}>Search Result</Text>
          </View>
          <View style={styles.searchBox}>
            <ScrollView>
              {tags.map((item, i) => (
                <TouchableOpacity
                  key={"tags" + i}
                  onPress={this.chooseTag.bind(null, item)}
                >
                  <View style={styles.tagPickItem}>
                    <View style={styles.tagPickItemInner}>
                      <Image source={tagIcon} style={{ marginTop: 0 }} />
                      <View style={styles.searchTagDetail}>
                        <Text
                          style={{ color: "rgb(255,255,255)", fontSize: 14 }}
                        >
                          {item.tagName}
                        </Text>
                        <Text
                          style={{
                            color: "rgba(255,255,255,0.4)",
                            fontSize: 12
                          }}
                        >
                          {item.sortNumber + "k+ Discussed"}
                        </Text>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        </View>
      </View>
    );
  };

  render() {
    const list = new Array(10).fill({ name: "Givenchy" });
    const { cancel } = this.props;
    const { searchName, defaulTags, searchTag } = this.state;
    let searchJudge = searchName.trim().length;
    return (
      <View style={styles.container}>
        <BlurView style={styles.filterContainer} blurAmount={10} />
        <View style={styles.searchContainer}>
          <SearchBar
            round
            showLoading
            containerStyle={styles.searchBarContainer}
            inputContainerStyle={styles.inputContainerStyle}
            inputStyle={styles.inputStyle}
            ref="searchBar"
            placeholder="Search tags and brands"
            value={searchName}
            platform="ios"
            clearIcon={searchName.length ? { name: "clear" } : false}
            cancelButtonTitle="Cancel"
            onChangeText={this.searchTagByName}
            onClear={this.clearSearchTagName}
          />
          <TouchableOpacity onPress={cancel}>
            <View style={styles.cancelStyle}>
              <Text style={{ color: "#fff", fontSize: 14 }}>Cancel</Text>
            </View>
          </TouchableOpacity>
        </View>

        {searchJudge ? (
          this.renderSearchTags(searchTag)
        ) : (
          <View>
            <View style={styles.historyContainer}>
              <View>
                <Text style={{ fontSize: 14, color: "#fff" }}>History</Text>
              </View>
              <TouchableOpacity onPress={this.clearHistoryTag}>
                <Image source={deleteIcon} style={{ marginTop: 5 }} />
              </TouchableOpacity>
            </View>
            <View style={styles.tagList}>
              {defaulTags.history.map((item, i) => (
                <TouchableOpacity
                  key={"tags-history" + i}
                  onPress={this.chooseTag.bind(null, item)}
                >
                  <TagItem name={item.tagName} />
                </TouchableOpacity>
              ))}
            </View>

            <View style={styles.historyContainer}>
              <View>
                <Text style={{ fontSize: 14, color: "#fff" }}>Hot</Text>
              </View>
            </View>
            <View style={styles.tagList}>
              {defaulTags.hot.map((item, i) => (
                <TouchableOpacity
                  key={"tags-hot" + i}
                  onPress={this.chooseTag.bind(null, item)}
                >
                  <TagItem name={item.tagName} />
                </TouchableOpacity>
              ))}
            </View>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    backgroundColor: "#F5FCFF",
    width,
    height,
    backgroundColor: "rgba(0,0,0,0.5)"
  },
  filterContainer: {
    position: "absolute",
    top: 0,
    left: 0,
    bottom: 0,
    right: 0
  },

  searchResultContainer: {
    display: "flex",
    flexDirection: "column",
    // justifyContent: "center",
    alignItems: "center",
    marginTop: 10,
    paddingLeft: 16,
    paddingRight: 16,
    width,
    height: height - 120
  },
  tagPickItem: {
    display: "flex",
    justifyContent: "center",
    width: width - 32,
    height: 50,
    backgroundColor: "rgba(216,216,216,0.3)",
    marginTop: 10,
    borderRadius: 25
  },

  tagPickItemInner: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    marginLeft: 15,
    width: width - 90,
    height: 40
  },
  searchTagDetail: {
    display: "flex",
    justifyContent: "center",
    marginLeft: 10,
    alignSelf: "center",
    width: width - 150,
    height: 40
  },

  searchContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 51,
    width,
    height: 50,
    paddingLeft: 5,
    paddingRight: 16
  },
  searchBox: {
    display: "flex",
    alignItems: "center",
    width: width - 32,
    height: height - 300
  },
  searchBarContainer: {
    width: width - 90,
    height: 50,
    borderTopColor: "transparent",
    borderBottomColor: "transparent",
    backgroundColor: "transparent"
  },
  inputContainerStyle: {
    width: width - 90,
    height: 50,
    padding: 0,
    marginLeft: 0,
    borderWidth: 0,
    borderColor: "transparent"
  },
  inputStyle: {
    width: width - 90,
    height: 30,
    backgroundColor: "rgba(255,255,255,0.1)",
    color: "rgba(255,255,255,0.8)",
    fontSize: 15
  },
  cancelStyle: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: 50,
    height: 50
  },
  historyContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    marginTop: 10,
    paddingLeft: 16,
    paddingRight: 16,
    width,
    height: 30
  },
  tagList: {
    width,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    paddingLeft: 16,
    paddingRight: 16,
    flexWrap: "wrap"
  },
  tagContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 10,
    marginBottom: 10,
    height: 26,
    borderRadius: 13,
    backgroundColor: "rgba(216,216,216,0.3)"
  }
});
