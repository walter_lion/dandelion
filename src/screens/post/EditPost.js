const back = require("../../assets/icons/back.png");
const down_arrow = require("../../assets/icons/down_arrow.png");
const tagIcon = require("../../assets/icons/tag.png");

import SelectTags from "./SelectTags";
import React, { Component } from "react";
import {
  Animated,
  Dimensions,
  Image,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  TextInput,
  TouchableWithoutFeedback,
  ActivityIndicator,
  Alert,
  DeviceInfo
} from "react-native";

import { Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
import {
  uploadImage,
  creatNewPost,
  updatePost,
  saveTag
} from "../../actions/post";
import Overlay from "../../components/custom/Overlay";
import { assetRoot } from "../../constants/urlConstant";
import Picker from "react-native-picker";
import * as Progress from "react-native-progress";

import uniqBy from "../../utils/lodash/uniqBy";

const { width, height } = Dimensions.get("window");
const X_WIDTH = 375;
const X_HEIGHT = 812;
const isIphoneX = height / width === X_HEIGHT / X_WIDTH;

const Icon = () => {
  return (
    <View style={styles.sendIcon}>
      <Image
        style={styles.sendIcon}
        source={require("../../assets/icons/send.png")}
      />
    </View>
  );
};

const Header = ({ styles, opacity, leftEvent, rightEvent, title }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image source={back} />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const Arrow = () => (
  <Image style={{ marginTop: 15, marginRight: 20 }} source={down_arrow} />
);

const TagItem = ({ id, name, deleteHandle }) => {
  return (
    <TouchableOpacity onPress={deleteHandle}>
      <View style={styles.tagContainer}>
        <Text style={styles.tagLabel}>#</Text>
        <Text style={{ fontSize: 12, marginLeft: 6 }}>{name}</Text>
      </View>
    </TouchableOpacity>
  );
};

export default class EditPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animatedY: new Animated.Value(-height),
      progress: 0,
      indeterminate: true,
      submitable: false,
      fetching: false,
      postId: null,
      imageList: [],
      selectedCatalog: null,
      postTitle: "",
      postContent: "",
      choosedTags: [],
      showBlock: false,
      block: false
    };
  }

  changeShowBlock = type => {
    this.setState({
      showBlock: type
    });
  };

  showPickerCatalog = () => {
    Picker.init({
      pickerData: ["travel", "lifestyle", "beauty", "fashion"],
      pickerFontColor: [0, 0, 0, 1],
      pickerTitleText: "",
      selectedValue: [
        this.state.selectedCatalog == null
          ? "travel"
          : this.state.selectedCatalog
      ],
      onPickerConfirm: pickedValue => {
        this.setState({
          selectedCatalog: pickedValue[0],
          choosedTags: []
        });
      },
      onPickerCancel: pickedValue => {
        // console.log("area", pickedValue);
      },
      onPickerSelect: pickedValue => {
        this.setState({
          selectedCatalog: pickedValue[0],
          choosedTags: []
        });
      }
    });
    Picker.show();
  };

  handleSubmit = () => {
    const {
      submitable,
      fetching,
      imageList,
      postTitle,
      postContent
    } = this.state;

    if (postTitle.trim() === "" || postContent.trim() === "") {
      return;
    }
    if (!submitable && !fetching) {
      this.setState({
        fetching: true
      });
      this.progressAnimate();
      const needLoads = imageList.filter(
        item => !item.postPhotoId && !item.url.includes("post_photos/")
      );
      if (needLoads.length > 0) {
        this.uploadImagehandle().then(res => {
          // Alert.alert(res);
          return this.uploadFormData(res).catch(res => {
            this.setState({
              fetching: false
            });
          });
        });
      } else {
        this.uploadFormData(imageList).catch(res => {
          this.setState({
            fetching: false
          });
        });
      }
    }
  };

  uploadFormData = res => {
    const {
      selectedCatalog,
      choosedTags,
      postId,
      postTitle,
      postContent
    } = this.state;
    const { navigation } = this.props;
    const photoCount = res.length;
    if (postId == null) {
      return creatNewPost({
        category: selectedCatalog,
        photoCount,
        tags: choosedTags,
        title: postTitle,
        content: postContent,
        photos: res
      }).then(res => {
        const { code, data } = res;
        if (code === 200) {
          this.setState(
            {
              progress: 1,
              fetching: false,
              block: true
            },
            () => {
              navigation.navigate("Home");
            }
          );
        }
      });
    } else {
      return updatePost({
        postId,
        photoCount,
        category: selectedCatalog,
        tags: choosedTags,
        title: postTitle,
        content: postContent,
        photos: res
      }).then(res => {
        const { code, data } = res;
        if (code === 200) {
          this.setState(
            {
              progress: 1,
              fetching: false
            },
            () => {
              navigation.navigate("Home");
            }
          );
        }
      });
    }
  };

  uploadImagehandle = () => {
    const { imageList } = this.state;
    const localImages = imageList
      .map((item, index) => ({ ...item, seq: index }))
      .filter(item => !item.postPhotoId && !item.url.includes("post_photos/"));
    const needLoads = localImages.map(item => item.url);
    const seqs = localImages.map(item => item.seq);
    return uploadImage(localImages).then(res => {
      const { code, data } = res;
      if (code === 200) {
        const resultImgaes = data;
        const imageDatas = imageList.map((item, index) => {
          let sq = seqs.indexOf(index);
          return sq !== -1 ? { ...item, url: resultImgaes[sq] } : item;
        });
        this.setState({
          progress: 0.6
        });
        return imageDatas;
      } else {
        return Promise.reject(code);
      }
    });
  };

  searchTag = () => {
    Animated.spring(this.state.animatedY, {
      toValue: 0,
      duration: 2000
    }).start();
  };

  chooseTagHandle = tag => {
    this.setState(
      {
        choosedTags: uniqBy(
          this.state.choosedTags.concat(tag),
          item => item.tagId
        )
      },
      () => {
        this.cancelSearchTag();
      }
    );
  };

  cancelSearchTag = () => {
    Animated.spring(this.state.animatedY, {
      toValue: -height,
      duration: 2000
    }).start();
  };

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  componentDidMount() {
    const { navigation } = this.props;
    const {
      state: {
        params: { imageList, postDetail }
      }
    } = navigation;
    let changeData = { imageList: this.state.imageList.concat(imageList) };
    if (postDetail) {
      changeData.postId = postDetail.postId;
      changeData.postTitle = postDetail.title;
      changeData.postContent = postDetail.content;
      changeData.choosedTags = postDetail.tags || [];
      changeData.selectedCatalog =
        changeData.choosedTags.length > 0 ? postDetail.category : null;
    }
    this.setState(changeData);
  }

  progressAnimate = () => {
    let progress = 0;
    const timer = setInterval(() => {
      progress += Math.random() / 5;
      if (progress >= 0.6 && this.state.progress < 0.6) {
        progress = 0.59;
      }

      if (progress >= 1) {
        progress = 0.9;
        clearInterval(timer);
      }
      this.setState({ progress });
    }, 500);
  };

  loseFocus = () => {
    this.refs.postContent.blur();
    this.refs.postTitle.blur();
  };

  changeTitle = event => {
    const postTitle = event.nativeEvent.text;
    this.setState({
      postTitle
    });
  };

  changeContent = event => {
    const postContent = event.nativeEvent.text;
    this.setState({
      postContent
    });
  };

  setCover = url => {
    let { imageList } = this.state;
    imageList = imageList.map(item => ({
      ...item,
      isSurface: item.url === url ? 1 : 0
    }));
    this.setState({ imageList });
  };

  deleteTag = tagId => {
    Alert.alert(
      "", //提示标题
      `Confirm delete ? `, //提示内容
      [
        { text: "cancel", onPress: () => {} },
        {
          text: "confirm",
          onPress: () => {
            this.setState({
              choosedTags: this.state.choosedTags.filter(
                item => item.tagId !== tagId
              )
            });
          }
        }
      ] //按钮集合
    );
  };

  render() {
    const {
      animatedY,
      submitable,
      choosedTags,
      imageList,
      selectedCatalog,
      postTitle,
      postContent,
      fetching,
      progress,
      indeterminate,
      showBlock,
      block
    } = this.state;

    const avaliableSubmit =
      postTitle.trim() !== "" && postContent.trim() !== "";
    return (
      <View style={styles.container}>
        <Header styles={headerStyles} leftEvent={this.goBack} />
        {fetching && (
          <Progress.Bar
            width={width}
            height={5}
            color={"rgb(253,92,99)"}
            useNativeDriver={true}
            progress={progress}
            borderRadius={0}
            indeterminate={false}
          />
        )}
        <View style={styles.wrapper}>
          <TouchableWithoutFeedback onPressOut={this.showPickerCatalog}>
            <View style={styles.catalog}>
              <Text
                style={{
                  ...styles.catalogInput,
                  color:
                    selectedCatalog === null
                      ? "rgb(206,206,206)"
                      : "rgb(51,51,51)"
                }}
              >
                {selectedCatalog === null
                  ? "To which category?"
                  : selectedCatalog}
              </Text>

              <Arrow />
            </View>
          </TouchableWithoutFeedback>
          <View style={styles.tagsWrapper}>
            <ScrollView horizontal>
              {selectedCatalog !== null && (
                <TouchableOpacity onPress={this.searchTag}>
                  <View
                    style={{
                      ...styles.tagContainer,
                      backgroundColor: "rgb(253,92,99)"
                    }}
                  >
                    <Text style={{ ...styles.tagLabel, color: "#fff" }}>+</Text>
                    <Text
                      style={{ fontSize: 12, marginLeft: 6, color: "#fff" }}
                    >
                      Add tags
                    </Text>
                  </View>
                </TouchableOpacity>
              )}
              {choosedTags.map((item, i) => (
                <TagItem
                  key={i}
                  name={item.tagName}
                  deleteHandle={this.deleteTag.bind(null, item.tagId)}
                />
              ))}
            </ScrollView>
          </View>
          <View
            style={{
              ...styles.postContainer,
              height: fetching ? height - 355 : height - 350
            }}
          >
            <View style={styles.titleBox}>
              <TextInput
                placeholder="What’s your Title?"
                ref="postTitle"
                multiline={true}
                style={styles.title}
                maxLength={150}
                value={postTitle}
                onEndEditing={this.changeTitle}
                onFocus={this.changeShowBlock.bind(null, true)}
                onBlur={this.changeShowBlock.bind(null, false)}
              />
            </View>
            <TextInput
              placeholder="What do you want to share?"
              ref="postContent"
              multiline={true}
              style={{ ...styles.textarea, height: showBlock ? 80 : 300 }}
              maxLength={1500}
              value={postContent}
              onEndEditing={this.changeContent}
              onFocus={this.changeShowBlock.bind(null, true)}
              onBlur={this.changeShowBlock.bind(null, false)}
            />
            {showBlock && (
              <TouchableWithoutFeedback onPress={this.loseFocus}>
                <View
                  style={{
                    ...styles.block,
                    height: isIphoneX ? 210 : 130
                  }}
                >
                  <View style={styles.confirmBox}>
                    <Text
                      style={{
                        textAlign: "center",
                        fontSize: 16,
                        color: "#fff"
                      }}
                    >
                      Quit Keyboard
                    </Text>
                  </View>
                </View>
              </TouchableWithoutFeedback>
            )}
          </View>
          <View style={styles.imageList}>
            <ScrollView horizontal>
              {imageList.map((image, i) => (
                <View style={styles.imageContainer} key={i}>
                  {image.isSurface === 1 && (
                    <View style={styles.coverContainer}>
                      <Text style={{ color: "#fff", fontSize: 13 }}>Cover</Text>
                    </View>
                  )}
                  <TouchableOpacity
                    onPress={this.setCover.bind(null, image.url)}
                  >
                    <Image
                      style={styles.imageItem}
                      source={{
                        uri: image.postPhotoId
                          ? `${assetRoot}/${image.url}`
                          : image.url
                      }}
                    />
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          </View>
        </View>
        <View style={styles.sendButton}>
          <Button
            icon={<Icon />}
            iconComponent={Icon}
            onPress={this.handleSubmit}
            title="Post"
            fontSize={18}
            color="#fff"
            buttonStyle={Object.assign({}, styles.sendButtonInner, {
              backgroundColor: avaliableSubmit
                ? "rgb(253,92,99)"
                : "rgb(223,223,223)"
            })}
          />
        </View>
        {!block && (
          <Animated.View
            style={{
              position: "absolute",
              width,
              height,
              transform: [
                {
                  translateY: animatedY
                }
              ]
            }}
          >
            <SelectTags
              category={this.state.selectedCatalog}
              cancel={this.cancelSearchTag}
              chooseTagHandle={this.chooseTagHandle}
            />
          </Animated.View>
        )}
        {fetching && (
          <Overlay
            isVisible={true}
            windowBackgroundColor="rgba(255, 255, 255, .5)"
            overlayBackgroundColor="rgba(255, 255, 255, .5)"
            width="auto"
            height="auto"
          >
            <ActivityIndicator size="large" color="rgb(253,92,99)" />
          </Overlay>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // display: 'flex',
    backgroundColor: "#F5FCFF"
  },
  wrapper: {
    position: "relative",
    display: "flex",
    alignItems: "center",
    width,
    height: height - 88,
    backgroundColor: "rgb(251,251,252)"
  },
  catalog: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    width: width - 20,
    height: 40,
    alignSelf: "center",
    backgroundColor: "rgb(255,255,255)",
    borderRadius: 4,
    paddingLeft: 14,
    paddingRight: 14,
    marginBottom: 10
  },
  catalogInput: {
    width: width - 50,
    height: 40,
    lineHeight: 40,
    color: "rgb(51,51,51)",
    fontSize: 16
  },

  postContainer: {
    width: width - 20,
    height: height - 350,
    alignSelf: "center",
    backgroundColor: "rgb(255,255,255)",
    borderRadius: 4,
    borderBottomColor: "rgb(242,242,242)",
    borderBottomWidth: 1
  },
  titleBox: {
    width: width - 20,
    height: 75,
    borderBottomColor: "rgb(242,242,242)",
    borderBottomWidth: 1
  },
  title: {
    marginTop: 8,
    width: width - 44,
    height: 40,
    alignSelf: "center",
    overflow: "hidden",
    fontSize: 16,
    fontWeight: "bold"
  },
  textarea: {
    marginTop: 8,
    width: width - 44,
    height: 80,
    alignSelf: "center",
    overflow: "hidden",
    fontSize: 16
  },
  block: {
    position: "absolute",
    bottom: 0,
    marginTop: 10,
    width,
    height: 130,
    alignSelf: "center",
    overflow: "hidden",
    fontSize: 16,
    backgroundColor: "rgba(0,0,0,0.3)"
  },
  confirmBox: {
    width,
    height: 38,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  sendButton: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    left: 0,
    bottom: 0,
    width,
    height: 60
  },
  sendButtonInner: {
    textAlign: "center",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width,
    height: 60
  },
  sendIcon: {
    width: 23,
    height: 23,
    marginRight: 10
  },
  imageList: {
    paddingTop: 12,
    paddingLeft: 12,
    paddingRight: 12,
    width: width - 20,
    height: 109,
    backgroundColor: "rgb(255,255,255)",
    borderRadius: 4
  },
  imageContainer: {
    position: "relative",
    marginLeft: 12
  },
  coverContainer: {
    position: "absolute",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    zIndex: 2,
    bottom: 12,
    left: 0,
    borderBottomLeftRadius: 4,
    width: 50,
    height: 20,
    backgroundColor: "rgb(253,92,99)"
  },
  imageItem: {
    width: 85,
    height: 85,
    borderRadius: 4
  },
  tagsWrapper: {
    width: width - 50,
    height: 40,
    lineHeight: 40,
    color: "rgb(51,51,51)",
    fontSize: 16,
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    flexWrap: "wrap"
  },
  tagLabel: {
    color: "rgb(253,92,99)"
  },
  tagContainer: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "flex-start",
    alignItems: "center",
    paddingLeft: 10,
    paddingRight: 10,
    marginRight: 10,
    marginBottom: 10,
    height: 26,
    borderRadius: 13,
    backgroundColor: "rgba(216,216,216,0.3)"
  },
  openSearch: {
    width,
    height: 40,
    backgroundColor: "red"
  }
});
const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 18,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    height: 18
  },
  titleText: {
    fontSize: 18,
    fontWeight: "bold",
    color: "#fff"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});
