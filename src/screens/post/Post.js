import EditPost from './EditPost'
import SelectImage from './SelectImage'
import {createStackNavigator} from 'react-navigation';

const RootStack = createStackNavigator({
  SelectImage: {
    screen: SelectImage,
  },
  EditPost: {
    screen: EditPost,
  }

}, {
  headerMode: 'none',
  mode: 'card',
});

export default RootStack
