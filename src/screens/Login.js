import * as serviceTool from "../utils/serviceTool";
import React, { Component } from "react";
import { Dimensions, Image, StyleSheet, Text, View, Alert } from "react-native";
import { Button } from "react-native-elements";
import { TextField } from "react-native-material-textfield";
const registerBg = require("../assets/imgs/registerBj.jpg");

const { width, height } = Dimensions.get("window");
const fontSize = 16;

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      errorList: [],
      isLoading: false
    };
  }
  handleSubmit = () => {
    const { username, password, isLoading } = this.state;
    const { navigation } = this.props;
    if (isLoading) {
      return;
    }
    this.setState({
      isLoading: true
    });
    serviceTool
      .login(username, password)
      .then(res => {
        const { status } = res;
        if (status === 200) {
          this.setState({
            isLoading: false
          });
          navigation.navigate("Main");
        }
      })
      .catch(res => {
        const { _bodyText } = res;
        if (_bodyText) {
          const { error } = JSON.parse(_bodyText);
          this.setState({
            isLoading: false,
            errorList: [error]
          });
        } else {
          this.setState({
            isLoading: false
          });
        }
      });
  };
  goToSignUp = () => {
    const { navigation } = this.props;
    navigation.navigate("SignUp");
  };

  onInputUsername = username => {
    this.setState({
      username: username.trim(),
      errorList: []
    });
  };
  onInputPassword = password => {
    this.setState({
      password,
      errorList: []
    });
  };
  render() {
    const { isLoading, username, password, errorList } = this.state;
    return (
      <View style={styles.container}>
        <Image source={registerBg} style={styles.imgBackground} />
        <View style={styles.formView}>
          <View style={styles.headLabel}>
            <Text style={styles.welcome}>Log In</Text>
          </View>
          <TextField
            label="Email"
            autoCapitalize={"none"}
            containerStyle={{ margin: 5 }}
            inputContainerStyle={styles.inputContainer}
            baseColor={"rgba(255,255,255,0.5)"}
            tintColor={"#fff"}
            textColor={"#fff"}
            error={
              errorList.includes("unauthorized")
                ? "this account does not exist!"
                : null
            }
            value={username}
            onChangeText={this.onInputUsername}
          />
          <TextField
            label="Password"
            error={
              errorList.includes("invalid_grant")
                ? "please fill correct password!"
                : null
            }
            secureTextEntry={true}
            containerStyle={{ margin: 5 }}
            inputContainerStyle={styles.inputContainer}
            baseColor={"rgba(255,255,255,0.5)"}
            tintColor={"#fff"}
            textColor={"#fff"}
            value={password}
            onChangeText={this.onInputPassword}
          />
          <Button
            loading={isLoading}
            loadingProps={{ size: "large", color: "rgba(111, 202, 186, 1)" }}
            onPress={this.handleSubmit}
            title="Log In"
            fontSize={fontSize}
            color="rgb(253,92,99)"
            buttonStyle={styles.button}
          />
        </View>
        <View style={styles.tipInfo}>
          <Text style={{ fontSize, color: "#fff" }}>
            Don’t have an account?{" "}
          </Text>
          <Text
            style={{ fontSize, color: "rgb(253,92,99)" }}
            onPress={this.goToSignUp}
          >
            Sign Up
          </Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  button: {
    marginTop: 20,
    padding: 0,
    width: 263,
    height: 40,
    alignSelf: "center",
    backgroundColor: "#fff",
    borderRadius: 4
  },
  imgBackground: {
    width,
    height,
    backgroundColor: "transparent",
    position: "absolute"
  },
  headLabel: {
    display: "flex",
    alignSelf: "flex-start",
    width: 263,
    height: 50
  },
  formView: {
    marginTop: -100,
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    width: 263,
    height: 400
  },
  inputContainer: {
    width: 263,
    height: 60
  },
  welcome: {
    fontSize: 30,
    fontWeight: "bold",
    color: "#fff",
    textAlign: "left",
    margin: 0
  },
  tipInfo: {
    position: "absolute",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    flexDirection: "row",
    textAlign: "center",
    color: "#333333",
    bottom: 61
  },

  textStyle: {
    textAlign: "center"
  }
});
