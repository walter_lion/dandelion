const close = require("../assets/icons/cancel.png");
const down_arrow = require("../assets/icons/down_arrow.png");

import React, { Component } from "react";
import {
  CameraRoll,
  Dimensions,
  FlatList,
  Image,
  ScrollView,
  StyleSheet,
  Switch,
  Text,
  Alert,
  TouchableOpacity,
  TouchableWithoutFeedback,
  View
} from "react-native";
import { Button, ListItem } from "react-native-elements";
import Picker from "react-native-picker";
import * as serviceTool from "../utils/serviceTool";
const restrictW = width - 40;
const pushNoti = () => (
  <View style={styles.formItem2}>
    <Text>Push-notifications</Text>
    <Switch
      value={true}
      onValueChange={() => {}}
      onTintColor="rgb(44,197,50)"
      thumbTintColor="#fff"
    />
  </View>
);
const { width, height } = Dimensions.get("window");
var fetchParams = {
  first: 6,
  groupTypes: "All",
  assetType: "Photos"
};

const Header = ({ styles, opacity, leftEvent, rightEvent, title }) => {
  return (
    <View style={styles.header}>
      <TouchableOpacity onPress={leftEvent}>
        <View style={styles.back}>
          <Image
            style={{ width: 20, height: 20, marginTop: -2 }}
            source={close}
          />
        </View>
      </TouchableOpacity>
      <View style={styles.title}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
      <TouchableOpacity onPress={rightEvent}>
        <View style={styles.share}>
          <Text
            style={{
              color: "rgb(253,92,99)",
              fontSize: 14,
              fontWeight: "bold"
            }}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

const Arrow = () => <Image source={down_arrow} />;
const ImageItem = ({ url, selected, index }) => {
  return (
    <View style={styles.imageItem}>
      <View style={styles.imageSelected}>
        <View style={styles.dot}>
          <Text
            style={{
              width: 15,
              height: 15,
              textAlign: "center",
              color: "rgb(253,92,99)"
            }}
          >
            {index}
          </Text>
        </View>
      </View>
      <Image
        style={{ width: 85, height: 85, borderRadius: 4 }}
        source={{ uri: url }}
      />
    </View>
  );
};

export default class Setting extends Component {
  constructor() {
    super();
    this.state = {
      imageList: []
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    var _that = this;
  }

  goBack = () => {
    const { navigation } = this.props;
    navigation.goBack();
  };

  goChangePassword = () => {
    const { navigation } = this.props;
    navigation.navigate("ChangePassword");
  };

  logoutHandle = () => {
    const { navigation } = this.props;
    serviceTool.logout().then(res => {
      const { code } = res;
      if (code === 200) {
        storage.remove({
          key: "loginState"
        });
        navigation.navigate("Login");
      }
    });
  };

  delCart = () => {
    Alert.alert(
      "", //提示标题
      `Confirm logout? `, //提示内容
      [
        { text: "cancel", onPress: () => {} },
        {
          text: "confirm",
          onPress: () => {
            this.logoutHandle();
          }
        }
      ] //按钮集合
    );
  };

  render() {
    const { imageList } = this.state;
    return (
      <View style={styles.container}>
        <Header
          styles={headerStyles}
          title={"Setting"}
          leftEvent={this.goBack}
        />
        <View style={styles.wrapper}>
          <View style={styles.titleWrapper}>
            <Text style={{ color: "rgb(118,129,150)", fontSize: 14 }}>
              Setting
            </Text>
          </View>
          <View style={styles.selectContainer}>
            <View style={styles.formView}>
              <ListItem
                titleStyle={styles.itemTitle}
                containerStyle={styles.formItem}
                contentContainerStyle={styles.formItem}
                title={"Change Password"}
                onPress={this.goChangePassword}
              />
            </View>
          </View>
          <Button
            onPress={this.delCart}
            title="Log Out"
            fontSize={14}
            color="#fff"
            buttonStyle={styles.logoutButton}
          />
        </View>
      </View>
    );
  }
}

const headerStyles = StyleSheet.create({
  header: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "rgba(255,255,255,0.8)",
    width,
    height: 88,
    borderBottomWidth: 1,
    borderBottomColor: "rgba(0,0,0,0.1)"
  },
  back: {
    marginTop: 45,
    marginLeft: 20,
    width: 48,
    height: 18
  },
  title: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    marginTop: 45,
    height: 18
  },
  titleText: {
    fontSize: 14,
    fontWeight: "bold",
    textAlign: "center",
    color: "rgb(51,51,51)"
  },
  share: {
    marginTop: 45,
    marginRight: 20,
    width: 40,
    height: 18
  }
});

const styles = StyleSheet.create({
  container: {
    width,
    height,
    display: "flex",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  openCamera: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgb(253,92,99)"
  },
  mainContainer: {
    position: "relative",
    margin: 20,
    width: 200,
    height: 200,
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center"
  },
  titleWrapper: {
    width,
    height: 36,
    display: "flex",
    justifyContent: "center",
    alignItems: "flex-start",
    paddingLeft: 16,
    backgroundColor: "#fff"
  },
  imageMask: {
    position: "absolute",
    top: 0,
    left: 0,
    width: 200,
    height: 200,
    zIndex: 2,
    backgroundColor: "rgba(253,92,99,0.7)",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 4
  },
  photoIcon: {
    width: 32,
    height: 27
  },
  selectContainer: {
    width,
    height: height - 200,
    display: "flex",
    alignItems: "center"
    // backgroundColor:'red'
  },
  formView: {
    width: 263,
    height: 300,
    display: "flex",
    alignItems: "center"
    // backgroundColor:'black'
  },
  inputContainer: {
    width: 263,
    height: 60
  },
  imageItem: {
    position: "relative",
    margin: 2.5,
    borderRadius: 4,
    width: 85,
    height: 85
  },
  imageSelected: {
    position: "absolute",
    zIndex: 2,
    top: 0,
    left: 0,
    borderRadius: 4,
    width: 85,
    height: 85,
    backgroundColor: "rgba(253,92,99,0.5)",
    flex: 3,
    justifyContent: "center",
    alignItems: "center"
  },
  dot: {
    width: 20,
    height: 20,
    borderRadius: 10,
    backgroundColor: "#fff",
    display: "flex",
    justifyContent: "center",
    alignItems: "center"
  },
  wrapper: {
    flex: 1,
    width,
    height: height - 88,
    marginTop: 10
  },
  logoutButton: {
    marginBottom: 20,
    padding: 0,
    bottom: 0,
    width: 120,
    height: 40,
    backgroundColor: "rgb(253,92,99)",
    alignSelf: "center",
    borderRadius: 4
  },
  formItem: {
    width,
    height: 48,
    backgroundColor: "#fff",
    borderBottomColor: "transparent"
  },
  formItem2: {
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width,
    height: 48,
    backgroundColor: "#fff",
    paddingLeft: 16,
    paddingRight: 16
  },
  itemTitle: {
    color: "rgb(13,14,21)",
    fontSize: 14,
    marginLeft: 6
  }
});
