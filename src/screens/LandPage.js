import React, { Component } from "react";
import { StyleSheet, Text, View } from "react-native";
import FullScreen from "../components/phone/FullScreen";
import SplashScreen from "react-native-splash-screen";
import * as serviceTool from "../utils/serviceTool";
export default class LandPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOnce: false
    };
  }
  componentDidMount() {
    const { navigation } = this.props;
    storage
      .load({
        key: "loginState",
        autoSync: true
      })
      .then(res => {
        if (res) {
          serviceTool.login(res.username, res.password).then(res => {
            const { status } = res;
            if (status === 200) {
              SplashScreen.hide();
              navigation.navigate("Main");
            }
          });
        } else {
          this.setState({
            isOnce: true
          });
          SplashScreen.hide();
        }
      })
      .catch(res => {
        this.setState({
          isOnce: true
        });
        SplashScreen.hide();
      });
  }
  render() {
    const { navigation } = this.props;
    return (
      <View style={styles.container}>
        {this.state.isOnce ? (
          <FullScreen
            onPressLogin={() => navigation.navigate("Login")}
            onPressSignUp={() => navigation.navigate("SignUp")}
          />
        ) : (
          <View />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
