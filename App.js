import React, { Component } from "react";
import { Platform, StyleSheet, Text, View } from "react-native";
import { createStackNavigator, createSwitchNavigator } from "react-navigation";
import Instabug from "instabug-reactnative";

import LandPage from "./src/screens/LandPage";
import ChangePassword from "./src/screens/ChangePassword";
import Login from "./src/screens/Login";
import Main from "./src/screens/Main";
import Myprofile from "./src/screens/Myprofile";
import Notifications from "./src/screens/Notifications";
import OthersProfile from "./src/screens/OthersProfile";
import Poster from "./src/screens/Poster";
import Setting from "./src/screens/Setting";
import ForgetPassword from "./src/screens/ForgetPassword";
import SignUp from "./src/screens/SignUp";
import OtherReport from "./src/screens/reports/OtherReport";
import Report from "./src/screens/reports/Report";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu"
});

type Props = {};

const AuthStack = createStackNavigator(
  {
    LandPage: {
      screen: LandPage
    },
    Login: {
      screen: Login
    },

    SignUp: {
      screen: SignUp
    }
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

const MainRootStack = createStackNavigator(
  {
    Main: {
      screen: Main
    },

    Myprofile: {
      screen: Myprofile
    },

    OthersProfile: {
      screen: OthersProfile
    },

    Poster: {
      screen: Poster
    },

    Setting: {
      screen: Setting
    },

    ChangePassword: {
      screen: ChangePassword
    },

    ForgetPassword: {
      screen: ForgetPassword
    },

    Notifications: {
      screen: Notifications
    },
    Report: {
      screen: Report
    },
    OtherReport: {
      screen: OtherReport
    }
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

const RootStack = createSwitchNavigator(
  {
    AuthStack: {
      screen: AuthStack
    },
    MainRootStack: {
      screen: MainRootStack
    }
  },
  {
    mode: "card",
    headerMode: "none"
  }
);

export default class App extends Component<Props> {
  constructor(props) {
    super(props);
    Instabug.startWithToken("527f594c1da6fec1e597b6c745b8bcca", [
      Instabug.invocationEvent.shake
    ]);
  }
  render() {
    return <RootStack />;
  }
}
